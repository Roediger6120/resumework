<%-- 
    Document   : index
    Created on : Apr 3, 2016, 5:38:33 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <title>JSP Page</title>


    </head>
    <body>
    <center><h1>Software Craftsmanship Guild</h1></center>
    <br><br />

    <div class="col-md-4 col-md-offset-1">
        <form class= "form-horizontal" role ="form">
            <center><h3>Existing Users</h3></center>
            <br><br />
            <div class ="form-group">
                <label for="email" class="col-md-4 control-label">Email</label>
                <div class="col-md-8"><input type="email" class="form-control" id="email">
                </div>
            </div>
            <div class="form-group">
                <label for="pwd" class="col-md-4 control-label">Password</label>
                <div class="col-md-8"><input type="password" class="form-control" id="pwd">
                </div>
            </div>
            <div class="checkbox col-md-offset-4">
                <label><input type="checkbox"> Remember me</label>
            </div>
            <div class="checkbox col-md-3 col-md-offset-4">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
    <div class="col-md-4 col-md-offset-1">
        <form class= "form-horizontal" role ="form">
            <center><h3>New User? Register Now!</h3></center>
            <br><br />
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">Email</label>
                <div class="col-md-8"><input type="email" class="form-control" id="email">
                </div>
            </div>
            <div class="form-group">
                <label for="text"class="col-md-4 control-label">First Name</label>
                <div class="col-md-8"><input type="text" class="form-control" id="firstname">
                </div>
            </div>
            <div class="form-group">
                <label for="text" class="col-md-4 control-label">Last Name</label>
                <div class="col-md-8"><input type="text" class="form-control" id="lastname">
                </div>
            </div>
            <div class="form-group">
                <label for="pwd"class="col-md-4 control-label">Password</label>
                <div class="col-md-8"><input type="password" class="form-control" id="password">
                </div>
            </div>
            <div class="form-group">
                <label for="confirmpwd"class="col-md-4 control-label">Confirm Password</label>
                <div class="col-md-8"><input type="password" class="form-control" id="confirmpassword">
                </div>
            </div>
            <div class="col-md-3 col-md-offset-4">
                <button type="submit" class="btn btn-success">Register</button>
            </div>
        </form>
    </div>










</body>
</html>
