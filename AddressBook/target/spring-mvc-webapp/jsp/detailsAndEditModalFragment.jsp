        <!-- details modal -->
        <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog"
             aria-labelledby="detailsModalLabel" aria-hidden="true">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3 id="address-id">AddressId Goes Here</h3>
                        <table class="table table-bordered">
                            <tr>
                                <th>First Name:</th>
                                <td id="address-firstName">Address First Name Goes Here</td>
                            </tr>
                            <tr>
                                <th>Last Name:</th>
                                <td id="address-lastName">Address Last Name Goes Here</td>
                            </tr>
                            <tr>
                                <th>Street:</th>
                                <td id="address-street">Street Goes Here</td>
                            </tr>
                            <tr>
                                <th>City:</th>
                                <td id="address-city">City Goes Here</td>
                            </tr>
                            <tr>
                                <th>State:</th>
                                <td id="address-state">State Goes Here</td>
                            </tr>
                             <tr>
                                <th>Zip</th>
                                <td id="address-zip">Zip Goes Here</td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- edit modal -->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog"
             aria-labelledby="detailsModalLabel" aria-hidden="true">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="detailsModalLabel">Edit Address</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="address-id">AddressId Goes Here</h3>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="edit-firstName" class="col-md-4 control-label">First Name:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-firstName" placeholder="First Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-lastName" class="col-md-4 control-label">Last Name:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-lastName" placeholder="Last Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-street" class="col-md-4 control-label">Street</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-street" placeholder="Street"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-city" class="col-md-4 control-label">City</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-city" placeholder="City"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-state" class="col-md-4 control-label">State:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-state" placeholder="State"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-zip" class="col-md-4 control-label">Zip</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-zip" placeholder="Zip"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit" id="edit-button" class="btn btn-default" data-dismiss="modal">Edit Address</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" id="edit-addressId"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>