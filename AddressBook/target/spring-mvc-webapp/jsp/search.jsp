<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book Search</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>

                </ul>    
            </div>
         <div class="row">
                <div class="col-md-6">
                    <h2>Search Results</h2>
                    <%@include file="addressSummaryTableFragment.jsp" %>
                </div>
                <div class="col-md-6">
                    <h2>Search Addresses</h2>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="search-first-name" class="col-md-4 control-label">First Name:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-first-name" placeholder="First Name"/></div>
                        </div>
                        <div class="form-group">
                            <label for="search-last-name" class="col-md-4 control-label">Last Name:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-last-name" placeholder="Last Name"/></div>
                        </div>
                        <div class="form-group">
                            <label for="search-street" class="col-md-4 control-label">Street:</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-street" placeholder="Street"/></div>
                        </div>
                        <div class="form-group">
                            <label for="search-city" class="col-md-4 control-label">City</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-city" placeholder="City"/></div>
                        </div>
                        <div class="form-group">
                            <label for="search-state" class="col-md-4 control-label">State</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-state" placeholder="State"/></div>
                        </div>
                        <div class="form-group">
                            <label for="search-zip" class="col-md-4 control-label">Zip</label>
                            <div class="col-md-8"><input type="text" class="form-control" id="search-zip" placeholder="Zip"/></div>
                        </div>
                         <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit" id="search-button" class="btn btn-default">Search Addresses</button>
                            </div>
                        </div>
                    </form>
                </div>


            </div>

        </div>

        <%@include file="detailsAndEditModalFragment.jsp" %>

        <%@include file="scriptsFragment.jsp" %>

    </body>
</html>

