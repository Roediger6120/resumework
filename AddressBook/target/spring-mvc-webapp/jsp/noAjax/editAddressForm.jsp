<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displaySearchForm">Search</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displayAddressList">List Addresses</a></li>
                    <li role="presentation"class="active"><a href="${pageContext.request.contextPath}/newAddressForm">Add Address</a></li>
                </ul>    
            </div>

            <sf:form class="form-horizontal" role="form" modelAttribute="addressToEdit"
                  action="editAddress" method="POST">
                <div class="form-group">
                    <label form ="edit-first-name" class="col-md-4 control-label">First Name</label>
                    <div class ="col-md-8"/>
                    <sf:input type ="text" class="form-control" id="edit-first-name" path="firstName" placeholder="First Name"/>
                     <sf:errors path="firstName" cssClass="error" cssStyle="color:red"></sf:errors>
                </div>
        </div>
        <div class="form-group">
            <label form ="edit-last-name" class="col-md-4 control-label">Last Name</label>
            <div class ="col-md-8"/>
            <sf:input type ="text" class="form-control" id="edit-last-name" path="lastName" placeholder="Last Name"/>
             <sf:errors path="lastName" cssClass="error" cssStyle="color:red"></sf:errors>
        </div>
                </div>
                <div class="form-group">
                    <label form ="edit-street" class="col-md-4 control-label">Street</label>
                    <div class ="col-md-8"/>
                    <sf:input type ="text" class="form-control" id="edit-street" path="street" placeholder="Street"/>
                     <sf:errors path="street" cssClass="error" cssStyle="color:red"></sf:errors>
                </div>
            </div>
            <div class="form-group">
                <label form ="edit-city" class="col-md-4 control-label">City</label>
                <div class ="col-md-8"/>
                <sf:input type ="text" class="form-control" id="edit-city" path="city" placeholder="City"/>
                 <sf:errors path="city" cssClass="error" cssStyle="color:red"></sf:errors>
            </div>              
            </div>
            <div class="form-group">
                <label form="edit-state" class="col-md-4 control-label">State</label>
                <div class="col-md-8"/>
                <sf:input type ="text" class="form-control" id="edit-state" path="state" placeholder="State"/>
                 <sf:errors path="state" cssClass="error" cssStyle="color:red"></sf:errors>
            </div>
            </div>
            <div class="form-group">
                <label form="edit-zip" class="col-md-4 control-label">Zip</label>
                <div class="col-md-8"/>
                <sf:input type="text" class="form-control" id="edit-zip" path="zip" placeholder="Zip"/>
                 <sf:errors path="zip" cssClass="error" cssStyle="color:red"></sf:errors>
            </div>
            </div>
            <div class="form-group">
                <sf:hidden path="addressId"/>
                <div class="col-md-offset-4 col-md-8">
                    <button type="submit" id="edit-button" class="btn btn-default">Edit
                </div>   
            </div>
            </sf:form>
            </div>    



<!-- Placed at the end of the document so the pages load faster -->
<script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</body>
</html>

