<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"class="active"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displaySearchForm">Search</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displayAddressList">List Addresses</a></li>
                    <li role="presentation"class="active"><a href="${pageContext.request.contextPath}/newAddressForm">Add Address</a></li>
                </ul>    
            </div>

            <form class="form-horizontal" role="form"
                  action="addAddress" method="POST">
                <div class="form-group">
                    <label form ="add-first-name" class="col-md-4 control-label">First Name</label>
                    <div class ="col-md-8"/>
                    <input type ="text" class="form-control" id="add-first-name" name="firstName" placeholder="First Name"/>
                </div>
        </div>
        <div class="form-group">
            <label form ="add-last-name" class="col-md-4 control-label">Last Name</label>
            <div class ="col-md-8"/>
            <input type ="text" class="form-control" id="add-last-name" name="lastName" placeholder="Last Name"/>
        </div>
                </div>
                <div class="form-group">
                    <label form ="add-street" class="col-md-4 control-label">Street</label>
                    <div class ="col-md-8"/>
                    <input type ="text" class="form-control" id="add-street" name="street" placeholder="Street"/>
                </div>
            </div>
            <div class="form-group">
                <label form ="add-city" class="col-md-4 control-label">City</label>
                <div class ="col-md-8"/>
                <input type ="text" class="form-control" id="add-city" name="city" placeholder="City"/>
            </div>              
            </div>
            <div class="form-group">
                <label form="add-state" class="col-md-4 control-label">State</label>
                <div class="col-md-8"/>
                <input type ="text" class="form-control" id="add-state" name="state" placeholder="State"/>
            </div>
            </div>
            <div class="form-group">
                <label form="add-zip" class="col-md-4 control-label">Zip</label>
                <div class="col-md-8"/>
                <input type="text" class="form-control" id="add-zip" name="zip" placeholder="Zip"/>
            </div>
            </div>
            <div class="form-group">
             
                <div class="col-md-offset-4 col-md-8">
                    <button type="submit" id="add-button" class="btn btn-default">Add
                </div>   
            </div>

            </div>    



<!-- Placed at the end of the document so the pages load faster -->
<script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</body>
</html>

