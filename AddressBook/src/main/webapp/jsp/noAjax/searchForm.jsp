<<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                     <li role="presentation"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                 <li role="presentation"><a href="${pageContext.request.contextPath}/search2">Search</a></li>
                 <li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>
                 <li role="presentation"><a href="${pageContext.request.contextPath}/displayAddressList">List Addresses</a></li>
                 <li role="presentation"><a href="${pageContext.request.contextPath}/newAddressForm">Add Address</a></li>
                </ul>    
            </div>

            <form action="search2" method="POST">
                <input type="text" name="firstName" />
                <input type="text" name="lastName"/>
                <input type="text" name="street" />
                <input type="text" name="city" />
                <input type="text" name="state" />
                 <input type="text" name="zip" />
                <button type="submit">Search Address</button>
            </form>   


        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>