$(document).ready(function () {
    loadAddress();
    
    $('#add-button').click(function (event) {
        event.preventDefault();
        
        $.ajax({
            type: 'POST',
            url: 'address',
            data: JSON.stringify({
                firstName: $('#add-first-name').val(),
                lastName: $('#add-last-name').val(),
                street: $('#add-street').val(),
                city: $('#add-city').val(),
                state: $('#add-state').val(),
                zip: $('#add-zip').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dateType': 'json'     
        }).success(function (data, status) {
            $('#add-first-name').val('');
            $('#add-last-name').val('');
            $('#add-street').val('');
            $('#add-city').val('');
            $('#add-state').val('');
            $('#add-zip').val('');
            
            loadAddress();
        });
});

$('#edit-button').click(function (event) {
    
    event.preventDefault();
    
    $.ajax({
        type: 'PUT',
        url: 'address/' + $('#edit-addressId').val(),
        data: JSON.stringify({
            addressId: $('#edit-addressId').val(),
            firstName: $('#edit-firstName').val(),
            lastName: $('#edit-lastName').val(),
            street: $('#edit-street').val(),
            city: $('#edit-city').val(),
            state: $('#edit-state').val(),
            zip: $('#edit-zip').val() 
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dateType': 'json'
    }).success(function (data, status) {
        loadAddress();
    });
});

$('#search-button').click(function(event) {
    
    event.preventDefault();
    
    $.ajax({
        type:'POST',
        url:'search/addresses',
        data: JSON.stringify({
            firstNameSearch: $('#search-first-name').val(),
            lastNameSearch: $('#search-last-name').val(),
            streetSearch: $('#search-street').val(),
            citySearch: $('#search-city').val(),
            stateSearch: $('#search-state').val(),
            zipSearch: $('#search-zip').val(),
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dateType': 'json'
    }).success(function(data, status) {
        fillAddressTable(data, status);
    });
});

});

function loadAddress() {
    
    var aTableBody = $('#contentRows');
    aTableBody.empty();
    
    $.ajax({
        url: "addresses",
        type: "GET",
        headers: {
            'Accept': 'application/json'    
        }
    }).success(function (data, status) {
        fillAddressTable(data, status);
    });
}

function fillAddressTable(addressBook, status) {
    
    var aTableBody = $('#contentRows');
    aTableBody.empty();
    
    $.each(addressBook, function (index, address) {
        aTableBody.append($('<tr>')
                .append($('<td>')
                    .append($('<a>')
                      .attr({
                          'data-address-id': address.addressId,
                          'data-toggle': 'modal',
                          'data-target': '#detailsModal'
                      })
                      .text(address.firstName + ' ' + address.lastName)
                      )
          )
          .append($('<td>').text(address.state))
          .append($('<td>')
                  .append($('<a>')
                     .attr({
                         'data-address-id': address.addressId,
                         'data-toggle': 'modal',
                         'data-target': '#editModal'
                     })
                      .text('Edit')
                      )
                )
        .append($('<td>')
                .append($('<a>')
                        .attr({
                             'onClick': 'deleteAddress(' + address.addressId + ')'                    
                 })
                .text('Delete')        
                )
            )
     );
 });
         
}
                
 function deleteAddress(id) {
     
     var answer = confirm("Do you REALLY want to delete this address?");
     
     if (answer === true) {
         $.ajax({
             url: 'address/' + id,
             type: 'DELETE'
         }).success(function () {
             loadAddress();
         });
     }
 }
 
 $('#detailsModal').on('show.bs.modal', function(event) {
     
     var element = $(event.relatedTarget);
     var addressId = element.data('address-id');
     
     var modal = $(this);
     
     $.ajax({
         type: 'GET',
         url: 'address/' + addressId,
         headers: {
             'Accept': 'application/json'
         }
     }).success(function (address) {
         modal.find('#address-id').text(address.addressId);
         modal.find('#address-firstName').text(address.firstName);
         modal.find('#address-lastName').text(address.lastName);
         modal.find('#address-street').text(address.street);
         modal.find('#address-city').text(address.city);
         modal.find('#address-state').text(address.state);
         modal.find('#address-zip').text(address.zip);
     });
 });
 
 $('#editModal').on('show.bs.modal', function (event) {
     
     var element = $(event.relatedTarget);
     var addressId = element.data('address-id');
     
     var modal = $(this);
     
     $.ajax({
         type: 'GET',
         url: 'address/' + addressId,
         headers: {
             'Accept': 'application/json'
         }
     }).success(function (address) {
         
         modal.find('#address-id').text(address.addressId);
         modal.find('#edit-firstName').val(address.firstName);
         modal.find('#edit-lastName').val(address.lastName);
         modal.find('#edit-street').val(address.street);
         modal.find('#edit-city').val(address.city);
         modal.find('#edit-state').val(address.state);
         modal.find('#edit-zip').val(address.zip);
         modal.find('#edit-addressId').val(address.addressId);
     });
 });