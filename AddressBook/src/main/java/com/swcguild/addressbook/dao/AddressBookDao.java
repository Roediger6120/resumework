/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.addressbook.dao;

import com.swcguild.addressbook.model.Address;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 *
 * @author apprentice
 */
public interface AddressBookDao {
        
   
    public Address addAddress(Address address);

    public void removeAddress(int addressId);
 
    public void updateAddress(Address address);
 
    public List<Address> getAllAddresses();
    
    public Address getAddressById(int addressId);
    
    public List<Address> searchAddress(Map<SearchTerm, String> criteria);
    
    public List<Address> searchAddress(Predicate<Address> filter);
    
}

