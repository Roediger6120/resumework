/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.addressbook.dao;

import com.swcguild.addressbook.model.Address;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoImpl implements AddressBookDao {

    private Map<Integer, Address> addressMap = new HashMap<>();
    private static int addressIdCounter = 0;

    public AddressBookDaoImpl() {
        Address address = new Address();
        address.setFirstName("Mickey");
        address.setLastName("Mouse");
        address.setStreet("1 Disney Lane");
        address.setCity("Orlando");
        address.setState("Florida");
        address.setZip("32830");
        this.addAddress(address);
        
        address = new Address();
        address.setFirstName("Minnie");
        address.setLastName("Mouse");
        address.setStreet("1 Disney Lane");
        address.setCity("Orlando");
        address.setState("Florida");
        address.setZip("32830");
        this.addAddress(address);
        
        address = new Address();
        address.setFirstName("Donald");
        address.setLastName("Trump");
        address.setStreet("1 Crazy Lane");
        address.setCity("CrazyTown");
        address.setState("Demented");
        address.setZip("12345");
        this.addAddress(address);
        
        address = new Address();
        address.setFirstName("Alicia");
        address.setLastName("Florrick");
        address.setStreet("234 Awesome Drive Apt 909");
        address.setCity("Chicago");
        address.setState("Illinois");
        address.setZip("60290");
        this.addAddress(address);
        
        address = new Address();
        address.setFirstName("William");
        address.setLastName("Adama");
        address.setStreet("Admiral's Quarters");
        address.setCity("BattleStar Galactica");
        address.setState("OuterSpace");
        address.setZip("99999");
        this.addAddress(address);
        
        address = new Address();
        address.setFirstName("Samantha");
        address.setLastName("Carter");
        address.setStreet("StarGate");
        address.setCity("Colorado Springs");
        address.setState("Colorado");
        address.setZip("80840");
        this.addAddress(address);
        
    }
    @Override
    public Address addAddress(Address address) {
        address.setAddressId(addressIdCounter);
        addressIdCounter++;
        addressMap.put(address.getAddressId(), address);
        return address;
    }

    @Override
    public void removeAddress(int addressId) {
       addressMap.remove(addressId);
    }

    @Override
    public void updateAddress(Address address) {
        addressMap.put(address.getAddressId(), address);
    }

    @Override
    public List<Address> getAllAddresses() {
        return new ArrayList(addressMap.values());
    }

    @Override
    public Address getAddressById(int addressId) {
       return addressMap.get(addressId);
    }

    @Override
    public List<Address> searchAddress(Map<SearchTerm, String> criteria) {
       String firstNameCriteria = criteria.get(SearchTerm.FIRST_NAME);
       String lastNameCriteria = criteria.get(SearchTerm.LAST_NAME);
       String streetCriteria = criteria.get(SearchTerm.STREET);
       String cityCriteria = criteria.get(SearchTerm.CITY);
       String stateCriteria = criteria.get(SearchTerm.STATE);
       String zipCriteria = criteria.get(SearchTerm.ZIP);
       
       Predicate<Address>firstNamePred;
       Predicate<Address>lastNamePred;
       Predicate<Address>streetPred;
       Predicate<Address>cityPred;
       Predicate<Address>statePred;
       Predicate<Address>zipPred;
       
       Predicate<Address> allPass = (address) -> {return true;};
       
       firstNamePred = (firstNameCriteria == null || firstNameCriteria.isEmpty())
               ? allPass: (address) -> address.getFirstName().toLowerCase().contains(firstNameCriteria.toLowerCase());
       lastNamePred = (lastNameCriteria == null || lastNameCriteria.isEmpty())
               ? allPass: (address) -> address.getLastName().toLowerCase().contains(lastNameCriteria.toLowerCase());
       streetPred = (streetCriteria == null || streetCriteria.isEmpty())
               ? allPass: (address) -> address.getStreet().toLowerCase().contains(streetCriteria.toLowerCase());
       cityPred = (cityCriteria == null || cityCriteria.isEmpty())
               ? allPass: (address) -> address.getCity().toLowerCase().contains(cityCriteria.toLowerCase());
       statePred = (stateCriteria == null || stateCriteria.isEmpty())
               ? allPass: (address) -> address.getState().toLowerCase().contains(stateCriteria.toLowerCase());
       zipPred = (zipCriteria == null || zipCriteria.isEmpty())
               ? allPass: (address) -> address.getZip().toLowerCase().contains(zipCriteria.toLowerCase());
       
       return addressMap.values().stream()
               .filter(firstNamePred)
               .filter(lastNamePred)
               .filter(streetPred)
               .filter(cityPred)
               .filter(statePred)
               .filter(zipPred)
               .collect(Collectors.toList());            
    }

    @Override
    public List<Address> searchAddress(Predicate<Address> filter) {
        return addressMap.values().stream()
                .filter(filter)
                .collect(Collectors.toList());
    }

}
