/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.addressbook.controller;

import com.swcguild.addressbook.dao.AddressBookDao;
import com.swcguild.addressbook.dao.SearchTerm;
import com.swcguild.addressbook.model.Address;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

 
 @Controller
public class HomeControllerTake2 {

    private AddressBookDao dao;

    @Inject
    public HomeControllerTake2(AddressBookDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/displayAddressList", method = RequestMethod.GET)
    public String displayAddressBook(Model model) {
        List<Address> allAddresses = dao.getAllAddresses();
        model.addAttribute("addressList", allAddresses);
        return "noAjax/displayAddressList";
    }
       @RequestMapping(value="/displaySearchForm", method=RequestMethod.GET)
    public String displaySearchForm(Model model){
        return "noAjax/searchForm";
    }
    
    @RequestMapping(value="/search2", method=RequestMethod.POST)
    public String searchForm(Model model, HttpServletRequest req){
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String street = req.getParameter("street");
        String city = req.getParameter("city");
        String state = req.getParameter("state");
        String zip = req.getParameter("zip");
        
        Map<SearchTerm, String> criteriaForSearch = new HashMap<>();
        criteriaForSearch.put(SearchTerm.FIRST_NAME, firstName);
        criteriaForSearch.put(SearchTerm.LAST_NAME, lastName);
        criteriaForSearch.put(SearchTerm.STREET, street);
        criteriaForSearch.put(SearchTerm.CITY, city);
        criteriaForSearch.put(SearchTerm.STATE, state);
        criteriaForSearch.put(SearchTerm.ZIP, zip);
        
        List<Address> foundAddresses = dao.searchAddress(criteriaForSearch);
        model.addAttribute("addressList", foundAddresses);
        model.addAttribute("fromSearch", true);
        model.addAttribute("listSize", foundAddresses.size());
        
        return "noAjax/displayAddressList";
    }
    
    @RequestMapping(value = "/newAddressForm", method = RequestMethod.GET)
    public String displayNewAddressForm() {
        return "noAjax/newAddressForm";
    }

    @RequestMapping(value = "/addAddress", method = RequestMethod.POST)
    public String addNewAddress(HttpServletRequest req) {
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String street = req.getParameter("street");
        String city = req.getParameter("city");
        String state = req.getParameter("state");
        String zip = req.getParameter("zip");

        Address newAddress = new Address();
        newAddress.setFirstName(firstName);
        newAddress.setLastName(lastName);
        newAddress.setStreet(street);
        newAddress.setCity(city);
        newAddress.setState(state);
        newAddress.setZip(zip);

        dao.addAddress(newAddress);

        return "redirect:noAjax/displayAddressList";
    }

    @RequestMapping(value = "/addressDelete", method = RequestMethod.GET)
    public String deleteAddress(HttpServletRequest req) {
        String addressId = req.getParameter("aId");
        int sanitizedId = this.sanitizeAddressId(addressId);
        if (sanitizedId >= 0) {
            dao.removeAddress(sanitizedId);
        }
        return "redirect:noAjax/displayAddressList";
    }

    @RequestMapping(value = "/editAddress", method = RequestMethod.GET)
    public String displayEditForm(HttpServletRequest req, Model model) {
        int sanitizedId = this.sanitizeAddressId(req.getParameter("aId"));
        if (sanitizedId >= 0) {
            Address address = dao.getAddressById(sanitizedId);
            model.addAttribute("addressToEdit", address);
        }
        return "noAjax/editAddressForm";
    }

    @RequestMapping(value = "/editAddress", method = RequestMethod.POST)
    public String editModelAddress(@ModelAttribute("addressToEdit") Address address) {
        dao.updateAddress(address);
        return "redirect:noAjax/displayAddressList";
    }

    private int sanitizeAddressId(String addressIdParam) {
        int addressIdInt = -1;

        try {
            addressIdInt = Integer.parseInt(addressIdParam);
        } catch (NumberFormatException e) {
            addressIdInt = -2;
        }
        return addressIdInt;
    }
}
