package com.swcguild.addressbook.controller;

import com.swcguild.addressbook.dao.AddressBookDao;
import com.swcguild.addressbook.model.Address;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class HomeController {
    
    private AddressBookDao dao;
    
    @Inject
    public HomeController(AddressBookDao dao) {
        this.dao = dao;
    }
    
    
    @RequestMapping(value={"/", "/home"}, method=RequestMethod.GET)
    public String displayHomePage() {
        
        return "home";
    }
    @RequestMapping(value="/address/{addressId}", method=RequestMethod.GET)
    @ResponseBody
    public Address getTheAddress(@PathVariable("addressId")int id) {
        return dao.getAddressById(id);
    }
    
    @RequestMapping(value="/address", method=RequestMethod.POST)
    @ResponseBody @ResponseStatus(HttpStatus.CREATED)
    public Address createTheAddress(@Valid @RequestBody Address newAddress) {
        dao.addAddress(newAddress);
        return newAddress;
    }
    
    @RequestMapping(value="/address/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTheAddress(@PathVariable("id")int addressId) {
        dao.removeAddress(addressId);
        
    }
    @RequestMapping(value="/address/{aId}", method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateTheAddress(@PathVariable("aId") int id, @Valid @RequestBody Address edittedAddress) {
        
        edittedAddress.setAddressId(id);
        dao.updateAddress(edittedAddress);
    }
    
    @RequestMapping(value="/addresses", method=RequestMethod.GET)
    @ResponseBody
    public List<Address> getAllAddresses() {
        return dao.getAllAddresses();
    }
    
    
}
