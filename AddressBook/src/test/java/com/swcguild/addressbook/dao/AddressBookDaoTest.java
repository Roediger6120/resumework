/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.addressbook.dao;

import com.swcguild.addressbook.model.Address;

import java.util.HashMap;
import java.util.List;
import java.util.Map;



import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 *
 * @author apprentice
 */
public class AddressBookDaoTest {

    AddressBookDao dao;

    public AddressBookDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext cxt = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = cxt.getBean("dao", AddressBookDao.class);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getAllAddresses() {
     
        Address ad = new Address();
        ad.setFirstName("Paul");
        ad.setLastName("Bunyon");
        ad.setStreet("Big Blue");
        ad.setCity("Boston");
        ad.setState("Massachussettes");
        ad.setZip("12345");
        dao.addAddress (ad);
      
         Address ad2 = new Address();
        ad2.setFirstName("Buffalo");
        ad2.setLastName("Bill");
        ad2.setStreet("Horsey");
        ad2.setCity("Austin");
        ad2.setState("Texas");
        ad2.setZip("11122");
        dao.addAddress(ad2);
        List<Address> aList = dao.getAllAddresses();
        assertEquals(aList.size(), 8);
    }

//    @Test
    public void searchAddress() {
       
        Address ad = new Address();
        ad.setFirstName("Solomon");
        ad.setLastName("Kim");
        ad.setStreet("1234 Ferguson");
        ad.setCity("Cleveland");
        ad.setState("Ohio");
        ad.setZip("13333");
        dao.addAddress(ad);
        

         Address ad2 = new Address();
        ad2.setFirstName("Winona");
        ad2.setLastName("Wixson");
        ad2.setStreet("Sunny Street");
        ad2.setCity("Boston");
        ad2.setState("Massachussetes");
        ad2.setZip("56778");
        dao.addAddress(ad2);
      

       Address ad3 = new Address();
        ad3.setFirstName("Batman");
        ad3.setLastName("Jones");
        ad3.setStreet("Big Castle");
        ad3.setCity("Gothan");
        ad3.setState("New York");
        ad3.setZip("99999");
        dao.addAddress(ad3);
     
        Map<SearchTerm, String> criteria = new HashMap<>();
        
        criteria.put(SearchTerm.LAST_NAME, "Jones");
        List<Address> aList = dao.searchAddress(criteria);
        assertEquals(1, aList.size());
        assertEquals(ad3, aList.get(0));

       
        criteria.put(SearchTerm.LAST_NAME, "Poindexter");
        aList = dao.searchAddress(criteria);
        assertEquals(0, aList.size());

       
        criteria.put(SearchTerm.STREET, "Big Castle");
        aList = dao.searchAddress(criteria);
        assertEquals(1, aList.size());
        assertEquals(ad3, aList.get(0));

        
        criteria.put(SearchTerm.STATE, "Ohio");
        aList = dao.searchAddress(criteria);
        assertEquals(1, aList.size());
        assertEquals(ad, aList.get(0));
       
        criteria.put(SearchTerm.STATE, "Washington");
        aList = dao.searchAddress(criteria);
        assertEquals(0, aList.size());

       
    }
}
