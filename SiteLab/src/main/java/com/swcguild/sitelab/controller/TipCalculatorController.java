package com.swcguild.sitelab.controller;



import com.swcguild.sitelab.*;
import java.text.DecimalFormat;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller

public class TipCalculatorController {
        
    public TipCalculatorController() {
    }
    
    @RequestMapping(value="/tipcalculator", method=RequestMethod.GET)
    public String displayMenu() {
        return "tipcalculator";
    }
    @RequestMapping(value="/tipcalculator", method=RequestMethod.POST)
    public String runMenu(Model model, HttpServletRequest request) {
       DecimalFormat df = new DecimalFormat("####0.00");

        String stringAmount = request.getParameter("amount");
        String stringTipPercent = request.getParameter("tip%");
        if (stringAmount == null || stringTipPercent == null) {
            model.addAttribute("badInput", true);

        } else {
            try {
                double amount = Double.parseDouble(stringAmount);
                double tipPercent = Double.parseDouble(stringTipPercent);

                double tipAmount = amount * (tipPercent / 100);
                double total = tipAmount + amount;
                if (amount <= 0 || tipPercent <= 0) {
                    model.addAttribute("badInput", true);
                } else {

                    df.format(amount);
                    df.format(tipPercent);
                    df.format(tipAmount);
                    df.format(total);
                    model.addAttribute("amount", amount);
                    model.addAttribute("tipPercent", tipPercent);
                    model.addAttribute("tipAmount", tipAmount);
                   model.addAttribute("total", total);
                }
            } catch (NumberFormatException nfe) {
                model.addAttribute("badInput", true);
            }
        }
        return "tcresponse";
    }
}
