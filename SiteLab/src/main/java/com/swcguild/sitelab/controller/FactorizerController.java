package com.swcguild.sitelab.controller;

import com.swcguild.sitelab.*;
import com.swcguild.sitelab.model.FindFactors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FactorizerController {

    public FactorizerController() {
    }

    @RequestMapping(value= "/factorizer", method=RequestMethod.GET)
    public String displayFactorizer() {
        return "/factorizer";
    }
    @RequestMapping(value = "/factorizer", method = RequestMethod.POST)
    public String runFactorizer(Model model, HttpServletRequest request) {
        String answer = request.getParameter("userInput");
        if (answer == null) {
            model.addAttribute("bad input", true);
        } else {
            try {
                int intAnswer = Integer.parseInt(answer);

                FindFactors find = new FindFactors(intAnswer);
                model.addAttribute("findAnswers", find);
//            request.setAttribute("factors", find.getFactorList());
//        
//            request.setAttribute("isPerfect", find.getIsPerfect());
//            request.setAttribute("isPrime", find.getIsPrime());

            } catch (NumberFormatException ex) {
              model.addAttribute("bad input", true);

            }
        }
        return "factresponse";
    }
}
