package com.swcguild.sitelab.controller;



import com.swcguild.sitelab.*;
import com.swcguild.sitelab.model.EachYear;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller

public class InterestController {
        
    public InterestController() {
    }
    
    @RequestMapping(value="/interest", method=RequestMethod.GET)
    public String displayMenu() {
        return "/interest";
    }
    @RequestMapping(value="/interest", method=RequestMethod.POST)
    public String runMenu(Model model, HttpServletRequest request) {
         String stringInterest = request.getParameter("interestRate");
        String stringPrinciple = request.getParameter("principle");
        String stringNum = request.getParameter("years");
        ArrayList<EachYear> storedData = new ArrayList<>();
        
        
        if (stringInterest == null || stringPrinciple == null || stringNum == null) {
            model.addAttribute("badInput", true);
        } else {
            try {
                double interest = Double.parseDouble(stringInterest);
                double principle = Double.parseDouble(stringPrinciple);
                int numYears = Integer.parseInt(stringNum);

                if (interest <= 0 || principle <= 0 || numYears <= 0) {
                    model.addAttribute("badInput", true);
                } else {

                    int currentYear = 0;
                    double beginBalance = principle;
                    double endBalance = beginBalance;
                    double yearlyInterest;
                    
                    
                    for (int i = 0; i < numYears; i++) {
                        currentYear++;
                        beginBalance = endBalance;
                        for (int j = 0; j < 4; j++) {
                            endBalance = (endBalance * (1 + interest / 400));
                            
                        }
                        yearlyInterest = endBalance - beginBalance;
                        
                       
                      
                        EachYear eachOne = new EachYear(currentYear, beginBalance, endBalance, yearlyInterest);
                        storedData.add(eachOne);
                    }

                    model.addAttribute("calcs", storedData);

                }
            } catch (NumberFormatException nfe) {
                model.addAttribute("badInput", true);
            }
        }
        return "icresponse";
    }
}
