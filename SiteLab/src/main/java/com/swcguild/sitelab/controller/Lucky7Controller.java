package com.swcguild.sitelab.controller;




import com.swcguild.sitelab.model.Lucky7;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class Lucky7Controller {
        
    public Lucky7Controller() {
    }
    
    @RequestMapping(value="/lucky7", method=RequestMethod.GET)
    public String lucky7() {
            return "lucky7";
    }
      @RequestMapping(value="/lucky7", method=RequestMethod.POST)
    public String runLucky7(Model model, HttpServletRequest req) throws IOException {
                String answer = req.getParameter("userInput");
       
        if (answer == null) {
            model.addAttribute("badInput", true);
        } else {
            try {
                int intAnswer = Integer.parseInt(answer);
                
                if (intAnswer <= 0) {
                     model.addAttribute("badInput", true);
                } else {
                Lucky7 lucky = new Lucky7(intAnswer);
                
                model.addAttribute("calcs", lucky);
                }
            } catch (NumberFormatException ex) {
                model.addAttribute("badInput", true);

            }
        }
      
        
        return "lsresponse";
    }
}
