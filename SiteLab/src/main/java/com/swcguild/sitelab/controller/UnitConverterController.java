package com.swcguild.sitelab.controller;




import com.swcguild.sitelab.model.Currency;
import com.swcguild.sitelab.model.Mass;
import com.swcguild.sitelab.model.Temperature;
import com.swcguild.sitelab.model.Volume;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller

public class UnitConverterController {
        
    public UnitConverterController() {
    }
    
    @RequestMapping(value="/converter", method=RequestMethod.GET)
    public String displayForm() {
        return "/converter";
    }
    @RequestMapping(value="/converter", method=RequestMethod.POST)
    public String unitConverter(Model model, HttpServletRequest request) {
        String classType = request.getParameter("type");
        String convertFrom ="";
        String convertTo ="";
        String stringNumberToConvert = request.getParameter("userInput");
        double answer = 0;
        try {
            int numberToConvert = Integer.parseInt(stringNumberToConvert);
            model.addAttribute("originalValue", numberToConvert);
            if (classType.equalsIgnoreCase("temperature")) {
                Temperature temp = new Temperature();
                convertFrom = request.getParameter("temperaturetypefrom");
                model.addAttribute("convertFrom", convertFrom);
                convertTo = request.getParameter("temperaturetypeto");
                model.addAttribute("convertTo", convertTo);
                if (convertFrom.equalsIgnoreCase("kelvin")) {
                    switch (convertTo.toLowerCase()) {
                        case "celsius":
                            answer = temp.kelvinToCelsuis(numberToConvert);
                            break;
                        case "fahrenheit":
                            answer = temp.kelvinToFahrenheit(numberToConvert);
                        default:
                            model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("celsius")) {
                    switch (convertTo.toLowerCase()) {
                        case "kelvin":
                            answer = temp.celsiusToKelvin(numberToConvert);
                            break;
                        case "fahrenheit":
                            answer = temp.celsiusToFahrenheit(numberToConvert);
                            break;
                        default:
                           model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("fahrenheit")) {
                    switch (convertTo.toLowerCase()) {
                        case "celsius":
                            answer = temp.fahrenheitToCelsius(numberToConvert);
                            break;
                        case "kelvin":
                            answer = temp.fahrenheitToKelvin(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }
                } else {
                    model.addAttribute("badInput", true);
                }
            } else if (classType.equalsIgnoreCase("mass")) {
                Mass mass = new Mass();
                convertFrom = request.getParameter("massTypeFrom");
                model.addAttribute("convertFrom", convertFrom);
                convertTo = request.getParameter("massTypeTo");
                model.addAttribute("convertTo", convertTo);
                if (convertFrom.equalsIgnoreCase("pound")) {

                    switch (convertTo.toLowerCase()) {

                        case "ounce":
                            answer = mass.poundsToOunces(numberToConvert);
                            break;
                        case "gram":
                            answer = mass.poundToGram(numberToConvert);
                            break;
                        case "kilogram":
                            answer = mass.poundToKilogram(numberToConvert);
                            break;
                        default:
                           model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("ounce")) {

                    switch (convertTo.toLowerCase()) {

                        case "pound":
                            answer = mass.ouncesToPounds(numberToConvert);
                            break;
                        case "gram":
                            answer = mass.ouncesToGrams(numberToConvert);
                            break;
                        case "kilogram":
                            answer = mass.ouncesToKilogram(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("gram")) {

                    switch (convertTo.toLowerCase()) {

                        case "pound":
                            answer = mass.gramToPound(numberToConvert);
                            break;
                        case "ounce":
                            answer = mass.gramsToOunces(numberToConvert);
                            break;
                        case "kilogram":
                            answer = mass.gramToKilogram(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("kilogram")) {

                    switch (convertTo.toLowerCase()) {

                        case "pound":
                            answer = mass.kilogramToPound(numberToConvert);
                            break;
                        case "ounce":
                            answer = mass.kilogramToOunces(numberToConvert);
                            break;
                        case "gram":
                            answer = mass.kilogramToGram(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }
                }
            } else if (classType.equalsIgnoreCase("currency")) {
                Currency currency = new Currency();
                convertFrom = request.getParameter("currencytypefrom");
                model.addAttribute("convertFrom", convertFrom);
                convertTo = request.getParameter("currencytypeto");
                model.addAttribute("convertTo", convertTo);
                if (convertFrom.equalsIgnoreCase("dollar")) {

                    switch (convertTo.toLowerCase()) {

                        case "pound":
                            answer = currency.dollarToPounds(numberToConvert);
                            break;
                        case "rupees":
                            answer = currency.dollarToRupees(numberToConvert);
                            break;
                        case "peso":
                            answer = currency.dollarToPeso(numberToConvert);
                            break;
                        case "euro":
                            answer = currency.dollarToEuros(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("pound")) {

                    switch (convertTo.toLowerCase()) {

                        case "dollar":
                            answer = currency.poundsToDollars(numberToConvert);
                            break;
                        case "rupees":
                            answer = currency.poundsToRupees(numberToConvert);
                            break;
                        case "peso":
                            answer = currency.poundsToPeso(numberToConvert);
                            break;
                        case "euro":
                            answer = currency.poundsToEuros(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("rupees")) {

                    switch (convertTo.toLowerCase()) {

                        case "dollar":
                            answer = currency.rupeesToDollar(numberToConvert);
                            break;
                        case "pound":
                            answer = currency.rupeesToPound(numberToConvert);
                            break;
                        case "peso":
                            answer = currency.rupeesToPeso(numberToConvert);
                            break;
                        case "euro":
                            answer = currency.rupeesToEuro(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("peso")) {

                    switch (convertTo.toLowerCase()) {

                        case "dollar":
                            answer = currency.pesoToDollar(numberToConvert);
                            break;
                        case "pound":
                            answer = currency.pesoToPound(numberToConvert);
                            break;
                        case "rupee":
                            answer = currency.pesoToRupee(numberToConvert);
                            break;
                        case "euro":
                            answer = currency.pesoToEuro(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("euro")) {

                    switch (convertTo.toLowerCase()) {

                        case "dollar":
                            answer = currency.euroToDollar(numberToConvert);
                            break;
                        case "pound":
                            answer = currency.euroToPound(numberToConvert);
                            break;
                        case "rupee":
                            answer = currency.euroToRupee(numberToConvert);
                            break;
                        case "peso":
                            answer = currency.euroToPeso(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }
                } else {
                    model.addAttribute("badInput", true);
                }
            } else if (classType.equalsIgnoreCase("volume")) {
                Volume volume = new Volume();
                convertFrom = request.getParameter("volumetypefrom");
                model.addAttribute("convertFrom", convertFrom);
                convertTo = request.getParameter("volumetypeto");
                model.addAttribute("convertTo", convertTo);
                if (convertFrom.equalsIgnoreCase("pint")) {
                    switch (convertTo.toLowerCase()) {

                        case "cup":
                            answer = volume.pintToCup(numberToConvert);
                            break;
                        case "liter":
                            answer = volume.pintToLiter(numberToConvert);
                            break;
                        case "gallon":
                            answer = volume.pintToGallon(numberToConvert);
                            break;
                        default:
                           model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("cup")) {

                    switch (convertTo.toLowerCase()) {

                        case "pint":
                            answer = volume.cupToPint(numberToConvert);
                            break;
                        case "liter":
                            answer = volume.cupToLiter(numberToConvert);
                            break;
                        case "gallon":
                            answer = volume.cupToGallon(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("liter")) {

                    switch (convertTo.toLowerCase()) {

                        case "pint":
                            answer = volume.literToPint(numberToConvert);
                            break;
                        case "cup":
                            answer = volume.literToCup(numberToConvert);
                            break;
                        case "gallon":
                            answer = volume.literToGallon(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }
                } else if (convertFrom.equalsIgnoreCase("gallon")) {

                    switch (convertTo.toLowerCase()) {

                        case "pint":
                            answer = volume.gallonToPint(numberToConvert);
                            break;
                        case "cup":
                            answer = volume.gallonToCup(numberToConvert);
                            break;
                        case "liter":
                            answer = volume.gallonToLiter(numberToConvert);
                            break;
                        default:
                            model.addAttribute("badInput", true);
                    }

                }
            } else {
                model.addAttribute("badInput", true);
            }
           model.addAttribute("calc", answer); 
        } catch (NumberFormatException nfe) {
            model.addAttribute("badInput", true);

            
        }
        
        return "csresponse";
    }
}
