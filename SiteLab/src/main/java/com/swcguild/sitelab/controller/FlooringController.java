package com.swcguild.sitelab.controller;

import com.swcguild.sitelab.*;
import com.swcguild.sitelab.model.FlooringCalculator;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FlooringController {

    public FlooringController() {
    }

    @RequestMapping(value = "/flooring", method = RequestMethod.GET)
    public String displayMenu() {
        return "/flooring";
    }

    @RequestMapping(value = "/flooring", method = RequestMethod.POST)
    public String runMenu(Model model, HttpServletRequest request) {
        String widthString = request.getParameter("width");
        String lengthString = request.getParameter("length");
        String costString = request.getParameter("costSqFt");

        if (widthString == null || lengthString == null || costString == null) {
            model.addAttribute("bad input", true);
        } else {
            try {
                double width = Double.parseDouble(widthString);
                double length = Double.parseDouble(lengthString);
                double costSqFt = Double.parseDouble(costString);

                if (width <= 0 || length <= 0 || costSqFt <= 0) {
                    model.addAttribute("bad input", true);
                } else {
                    FlooringCalculator calc = new FlooringCalculator(width, length, costSqFt);
                    model.addAttribute("calculations", calc);
                    model.addAttribute("length", length);
                    model.addAttribute("costSqFt", costSqFt);
                    model.addAttribute("laborTime", calc.getLaborTime());
                    model.addAttribute("laborCost", calc.getLaborCost());
                    model.addAttribute("flooringCost", calc.getFlooringCost());
                    model.addAttribute("flooringArea", calc.getAreaSqFt());
                }
            } catch (NumberFormatException ex) {
                model.addAttribute("bad input", true);

            }
        }

        return "fcresponse";
    }
}
