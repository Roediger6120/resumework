/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.sitelab.model;

/**
 *
 * @author apprentice
 */
public class Temperature {

    private double numberToConvert;

    public Temperature() {
    }

    public Double celsiusToFahrenheit(double numberToConvert) {
      
       return (numberToConvert * 9 / 5) + 32;
    }

    public Double fahrenheitToCelsius(double numberToConvert) {
       
        return ((numberToConvert - 32) * 5 / 9);
    }

    public Double kelvinToCelsuis(double numberToConvert) {
        return numberToConvert - 273.15;
    }

    public Double celsiusToKelvin(double numberToConvert) {
        return numberToConvert + 273.15;
    }

    public Double fahrenheitToKelvin(double numberToConvert) {
        return (numberToConvert + 459.67) * 5 / 9;
    }

    public Double kelvinToFahrenheit(double numberToConvert) {
        return (numberToConvert * 9 / 5) - 459.67;
    }
}
