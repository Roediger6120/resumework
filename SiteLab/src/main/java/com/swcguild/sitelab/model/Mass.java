/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.sitelab.model;

/**
 *
 * @author apprentice
 */
public class Mass {
    public Double poundsToOunces(double numberToConvert) {
        return numberToConvert * 16;
    }
    public Double ouncesToPounds(double numberToConvert) {
        return numberToConvert * 0.0625;
    }
    public Double ouncesToGrams(double numberToConvert) {
        return numberToConvert/0.035274;
    }
    public Double gramsToOunces(double numberToConvert) {
        return numberToConvert * 0.035274;
    }
    public Double ouncesToKilogram(double numberToConvert) {
        return numberToConvert / 35.274;
    }
    public Double kilogramToOunces(double numberToConvert) {
        return numberToConvert * 35.274;
    }
    public Double poundToGram(double numberToConvert) {
        return numberToConvert / 0.0022046;
    }
    public Double gramToPound(double numberToConvert) {
        return numberToConvert * 0.0022046;
    }
    public Double poundToKilogram(double numberToConvert) {
        return numberToConvert / 2.2046;
    }
    public Double kilogramToPound (double numberToConvert) {
        return numberToConvert * 2.2046;
    }
    public Double gramToKilogram (double numberToConvert) {
        return numberToConvert * 1000;
    }
    public Double kilogramToGram (double numberToConvert) {
        return numberToConvert / 1000;
    }
}
