/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.sitelab.model;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class Lucky7 {

    private int maxMoney;
    private int rollCount;
    private int rollCountMax;
    private int die1;
    private int die2;

    public Lucky7(int startingMoney) {

        maxMoney = startingMoney;
        Random r = new Random();
        while (startingMoney >= 1) {

            die1 = r.nextInt(6) + 1;
            die2 = r.nextInt(6) + 1;
            rollCount++;
            if (die1 + die2 == 7) {
                startingMoney = startingMoney + 4;
            } else {
                startingMoney = startingMoney - 1;
            }
            if (startingMoney > maxMoney) {
                maxMoney = startingMoney;
                rollCountMax = rollCount;

            }
        }

    }

    public int getMaxMoney() {
        return maxMoney;
    }

    public int getRollCount() {
        return rollCount;
    }

    public int getRollCountMax() {
        return rollCountMax;
    }

    public int getDie1() {
        return die1;
    }

    public int getDie2() {
        return die2;
    }

}
