/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.sitelab.model;

/**
 *
 * @author apprentice
 */
public class Currency {
   
   public Double dollarToEuros(double numberToConvert) {
       return numberToConvert * .875575;
   }
   public Double dollarToPounds(double numberToConvert) {
       return numberToConvert * .70;
   }
   public Double dollarToRupees(double numberToConvert) {
       return numberToConvert * 66.23;
   }
   public Double dollarToPeso(double numberToConvert) {
       return numberToConvert * 17.34;
   }
   public Double poundsToDollars(double numberToConvert) {
       return numberToConvert * 1.42298;
   }
   public Double poundsToEuros(double numberToConvert) {
       return numberToConvert * 1.25;
   }
   public Double poundsToRupees(double numberToConvert) {
       return numberToConvert * 94.24;
   }
   public Double poundsToPeso(double numberToConvert) {
       return numberToConvert * 24.67;
   }
   public Double rupeesToDollar(double numberToConvert) {
       return numberToConvert * 0.015;
   }
   public Double rupeesToPound(double numberToConvert) {
       return numberToConvert * 0.011;
   }
   public Double rupeesToPeso(double numberToConvert) {
       return numberToConvert * 0.026;
   }
   public Double rupeesToEuro(double numberToConvert) {
       return numberToConvert * 0.013;
   }
   public Double pesoToDollar(double numberToConvert) {
       return numberToConvert * 0.058;
   }
    public Double pesoToPound(double numberToConvert) {
       return numberToConvert * 0.041;
   }
     public Double pesoToRupee(double numberToConvert) {
       return numberToConvert * 3.82;
   }
      public Double pesoToEuro(double numberToConvert) {
       return numberToConvert * 0.057;
   }
      public Double euroToDollar(double numberToConvert) {
       return numberToConvert * 1.14;
   }
    public Double euroToPound(double numberToConvert) {
       return numberToConvert * .80;
   }
     public Double euroToRupee(double numberToConvert) {
       return numberToConvert * 75.46;
   }
      public Double euroToPeso(double numberToConvert) {
       return numberToConvert * 19.76;
   }
   
    
    
    
    
}
