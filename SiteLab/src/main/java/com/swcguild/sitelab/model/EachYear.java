/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.sitelab.model;

/**
 *
 * @author apprentice
 */
public class EachYear {
    private int currentYear;
    private double beginBalance;
    private double endBalance;
    private double yearlyInterest;

    public EachYear(int currentYear, double beginBalance, double endBalance, double yearlyInterest) {
        this.currentYear = currentYear;
        this.beginBalance = beginBalance;
        this.endBalance = endBalance;
        this.yearlyInterest = yearlyInterest;
    }

    public EachYear() {
    }
    

    public int getCurrentYear() {
        return currentYear;
    }

    public double getBeginBalance() {
        return beginBalance;
    }
    public double getEndBalance() {
        return endBalance;
    }

    public double getYearlyInterest() {
        return yearlyInterest;
    }

}
