/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.sitelab.model;

/**
 *
 * @author apprentice
 */
public class Volume {
    public Double pintToCup (double numberToConvert) {
        return numberToConvert * 2;
    }
    public Double cupToPint (double numberToConvert) {
        return numberToConvert / 2;
    }
    public Double pintToLiter (double numberToConvert) {
        return numberToConvert / 2.1134;
    }
    public Double literToPint (double numberToConvert) {
        return numberToConvert * 2.1134;
    }
    public Double pintToGallon (double numberToConvert) {
        return numberToConvert * 8;
    }
    public Double gallonToPint (double numberToConvert) {
        return numberToConvert / 8;
    }
   
    public Double cupToLiter (double numberToConvert) {
        return numberToConvert / 4.2268;
    }
    public Double literToCup (double numberToConvert) {
        return numberToConvert * 4.2268;
    }
    public Double cupToGallon (double numberToConvert) {
        return numberToConvert * 16;
    }
    public Double gallonToCup (double numberToConvert) {
        return numberToConvert / 16;
    }
    public Double literToGallon (double numberToConvert) {
        return numberToConvert * .26417;
    }
    public Double gallonToLiter (double numberToConvert) {
        return numberToConvert / .26417;
    }
    
}
