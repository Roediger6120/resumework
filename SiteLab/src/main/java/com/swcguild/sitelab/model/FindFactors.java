/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.sitelab.model;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class FindFactors {


    private int numFactors;
    private int sumFactors = 1;
    private boolean isPrime;
    private boolean isPerfect;
   
    
    ArrayList<Integer> factorList = new ArrayList<>();
    
    public FindFactors(int num) {
        factorList.add(1);
        for (int i = 2; i <=num; i++) {
            if (num % i == 0) {
                factorList.add(i);
                numFactors = numFactors + 1;
                sumFactors = sumFactors + i;
            }
        }
        if (numFactors == 1) {
            isPrime = true;
        }
        if (sumFactors == num * 2) {
            isPerfect = true;
        }
        
    }

    public int getNumFactors() {
        return this.numFactors;
    }

    public int getSumFactors() {
        return this.sumFactors;

    }
    public boolean getIsPrime() {
        return this.isPrime;
    }
    public boolean getIsPerfect() {
        return this.isPerfect;
    }
    public ArrayList<Integer> getFactorList() {
        return this.factorList;
    }
}   


