<%-- 
    Document   : Home
    Created on : Apr 3, 2016, 6:45:55 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/homepage.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body style="" >
        <div class="container" id ="mainheader" style=" ">
            <center><h2>Software Craftsmanship Guild Java Cohort</h2>
                <h3>JSP Site Lab</h3></center>
            <hr/>
        </div>
        <div class="container" id="navbar">
            <div class="navbar">
                <ul class="nav nav-pills">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/home.jsp">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/lucky7">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/factorizer">Factorizer</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/interest">Interest Calculator</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/flooring">Flooring Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalculator">Tip Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/converter">Unit Converter</a></li>
                </ul>    
            </div>
        </div>
            <br><br />

            <div class="container">
                <div class="form-group col-md-6 col-md-offset-4">
                    <h3>Flooring Cost Calculator</h3>
                    <form method="POST" action="flooring">
                       
                        <div class= "form-group">
                            <label for="width" class="col-md-offset-2 col-md-2"> Width:</label>
                            <div class="col-md-4"> <input class= "form-control" type="number" min=5 max=1999 step=5 name="width"> ft.<br/>
                        </div>
                        </div>
                        <div class= "form-group">
                            <label for= "length" class="col-md-offset-2 col-md-2">Length:</label>
                            <div class ="col-md-8"><input type="number" min=5 max=1999 step=5 name="length"> ft.<br/>
                        </div>
                        </div>
                        <div class= "form-group">
                            <label for="costSqFt" class="col-md-offset-2 col-md-2">Cost / SqFt</label>
                            <div class="col-md-4"><input type="number" min=0 max=199 step=1 name="costSqFt"> $/sqft <br/>
                        </div>
                        </div>
                        <div class="form-group">    
                            <div class="col-md-6 col-md-offset-2"><button class = "btn btn-success" type ="submit" value="Submit">Submit</button></div>
                        </div>

                    </form>
                </div>
            </div>
           


        
        <footer class="footer">
            <div class="container">

                <p class="text-center text-muted">Created by Carrie Roediger 2016 </p>
                <p class="text-center text-muted"> Powered by Java and Bootstrap</p>

            </div>
        </footer> 
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/homepage.css"></script>



    </body>
</html>
