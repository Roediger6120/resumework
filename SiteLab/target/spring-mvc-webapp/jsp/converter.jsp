<%-- 
    Document   : Home
    Created on : Apr 3, 2016, 6:45:55 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/homepage.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    
      <script type="text/javascript">
     function displayCurrency() {
                document.getElementById("convertCurrency").style.display = "block";
                document.getElementById("convertMass").style.display = "none";
                document.getElementById("temperature").style.display = "none";
                document.getElementById("convertVolume").style.display = "none";

            }
            function displayMass() {
                document.getElementById("convertCurrency").style.display = "none";
                document.getElementById("convertMass").style.display = "block";
                document.getElementById("temperature").style.display = "none";
                document.getElementById("volume").style.display = "none";

            }
            function displayTemperature() {
                document.getElementById("convertCurrency").style.display = "none";
                document.getElementById("convertMass").style.display = "none";
                document.getElementById("convertTemp").style.display = "block";
                document.getElementById("convertVolume").style.display = "none";

            }
            function displayVolume() {
                document.getElementById("convertCurrency").style.display = "none";
                document.getElementById("convertMass").style.display = "none";
                document.getElementById("convertTemp").style.display = "none";
                document.getElementById("convertVolume").style.display = "block";

            }

        </script>  
        </head>
    <body style="" >
        <div class="container" id ="mainheader" style=" ">
            <center><h2>Software Craftsmanship Guild Java Cohort</h2>
                <h3>JSP Site Lab</h3></center>
            <hr/>
        </div>
        <div class="container" id="navbar">
            <div class="navbar">
                 <ul class="nav nav-pills">
                <li role="presentation"><a href="${pageContext.request.contextPath}/home.jsp">Home</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/lucky7">Lucky Sevens</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/factorizer">Factorizer</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/interest">Interest Calculator</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/flooring">Flooring Calculator</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalculator">Tip Calculator</a></li>
                <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/converter">Unit Converter</a></li>
                </ul>    
            
    
    
          <div class="container">   
            <h3> Please choose the type of units you want to convert</h3>
            <form method="POST" action="converter" class="form-horizontal">
                <div class="row">
                    <div class="col-md-8" id="typeConvert">
                        <input type="radio" name="type" value="currency" onclick="displayCurrency()"/>Currency<br>
                        <input type="radio" name="type" value="mass" onclick="displayMass()"/>Mass<br>
                        <input type="radio" name="type" value="temperature" onclick="displayTemperature()"/>Temperature<br>
                        <input type="radio" name="type" value="volume" onclick="displayVolume()"/>Volume<br>
                    </div>

                    <div id="unitTypes" class="row form-group">
                        <div class="col-md-8">
                            <div id="convertCurrency" style="display:none">
                                Unit to Convert From
                                <select name="currencytypefrom">
                                    <option value="dollar">Dollar</option>
                                    <option value="euro">Euro</option>
                                    <option value="peso">Peso</option>
                                    <option value="pound">Pound</option>
                                    <option value="rupee">Rupee</option>
                                </select> <br>
                                Unit to Convert To
                                <select name="currencytypeto">
                                    <option value="dollar">Dollar</option>
                                    <option value="euro">Euro</option>
                                    <option value="peso">Peso</option>
                                    <option value="pound">Pound</option>
                                    <option value="rupee">Rupee</option>
                                </select><br>

                            </div>
                            <div id="convertMass" style="display:none">
                                Unit to Convert From
                                <select name = massTypeFrom>
                                    <option value="ounce">Ounce</option>
                                    <option value="pound">Pound</option>
                                    <option value="gram">Gram</option>
                                    <option value="kilogram">Kilogram</option>
                                </select><br>
                                Unit to Convert To
                                <select name = massTypeTo>
                                    <option value="ounce">Ounce</option>
                                    <option value="pound">Pound</option>
                                    <option value="gram">Gram</option>
                                    <option value="kilogram">Kilogram</option>
                                </select><br>
                            </div> 
                            <div id="convertTemp" style="display:none">
                                Unit to Convert From
                                <select name="temperaturetypefrom">
                                    <option value="celsius">Celsius</option>
                                    <option value="fahrenheit">Fahrenheit</option>
                                    <option value="kelvin">Kelvin</option>
                                </select><br>
                                Unit to Convert To
                                <select name="temperaturetypeto">
                                    <option value="celsius">Celsius</option>
                                    <option value="fahrenheit">Fahrenheit</option>
                                    <option value="kelvin">Kelvin</option>

                                </select><br>
                            </div>
                            <div id="convertVolume" style="display:none">
                                Unit to Convert From
                                <select name="volumetypefrom">
                                    <option value="cup">Cup</option>
                                    <option value="pint">Pint</option>
                                    <option value="liter">Liter</option>
                                    <option value="gallon">Gallon</option>

                                </select><br>
                                Unit to Convert To
                                <select name="volumetypeto">
                                    <option value="cup">Cup</option>
                                    <option value="pint">Pint</option>
                                    <option value="liter">Liter</option>
                                    <option value="gallon">Gallon</option>

                                </select><br>
                            </div>
                        </div>
                    </div>
                        <br></br>
                        <div class ="row form-group">
                            Please enter value to convert:
                            <input type="number" name="userInput" min="0"/>
                        </div> 
                         <div class ="row form-group">
                             <button type="submit" value="convert" class="btn btn-info">Convert</button>
                        </div>
                    </div>
            </form>
          </div>
        </div>
    </div>
 
       
        

       <footer class="footer">
            <div class="container">
             
            <p class="text-center text-muted">Created by Carrie Roediger 2016 </p>
            <p class="text-center text-muted"> Powered by Java and Bootstrap</p>
            
            </div>
        </footer> 
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/homepage.css"></script>
         <script src="${pageContext.request.contextPath}/js/unitconverter.css"></script>
       
        
                 
    </body>
</html>
