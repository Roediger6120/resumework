<%-- 
    Document   : Home
    Created on : Apr 3, 2016, 6:45:55 PM
    Author     : apprentice
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/homepage.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body style="" >
        <div class="container" id ="mainheader" style=" ">
            <center><h2>Software Craftsmanship Guild Java Cohort</h2>
                <h3>JSP Site Lab</h3></center>
            <hr/>
        </div>
        <div class="container" id="navbar">
            <div class="navbar">
                 <ul class="nav nav-pills">
                <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/lucky7">Lucky Sevens</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/factorizer">Factorizer</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/interest">Interest Calculator</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/flooring">Flooring Calculator</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalculator">Tip Calculator</a></li>
                <li role="presentation"><a href="${pageContext.request.contextPath}/converter">Unit Converter</a></li>
                </ul>    
            </div>
                <br><br />
                
                <div class="container">
                    <div class="col-md-6 col-md-offset-4">
                    <h3>Here are the results of your Flooring Order</h3>

                    <c:if test="${badInput == true}">
                        Please go back and provide actual correct input. 
                        <a href="flooring.jsp">Return to Flooring form.</a>
                    </c:if>

                    <c:out value="${width}"/>ft *
                    <c:out value="${length}"/>ft =
                    <c:out value="${flooringArea}" />sqft <br />

                    <c:out value="${flooringArea}" />sqft *
                    <c:out value="${costSqFt}" />sqft =
                    <c:out value="${flooringCost}"/>$ <br />

                    <c:out value="${flooringArea}" /> / 5 sqft/hr =
                    <c:out value="${laborTime}" />15m increment (rounded to next 15m increment)
                    <br />
                    <c:out value="${laborTime}" />15m increment * 86$/hr =
                    <c:out value="${laborCost}" />$
                    </div>
                </div>

      
        </div>
        <footer class="footer">
            <div class="container">
             
            <p class="text-center text-muted">Created by Carrie Roediger 2016 </p>
            <p class="text-center text-muted"> Powered by Java and Bootstrap</p>
            
            </div>
        </footer> 
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/homepage.css"></script>
       
        
                 
    </body>
</html>
