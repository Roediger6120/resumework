/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdlibrarywithwinona.controller;

import com.swcguild.dvdlibrarywithwinona.dao.DvdLibraryDao;
import com.swcguild.dvdlibrarywithwinona.dao.SearchTerm;
import com.swcguild.dvdlibrarywithwinona.model.Dvd;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class MainController {

    DvdLibraryDao dao;

    @Inject
    public MainController(DvdLibraryDao dao) {
        this.dao = dao;
    }

    

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayList() {
        return "list";
    }

    @ResponseBody
    @RequestMapping(value = "/library", method = RequestMethod.GET)
    public List<Dvd> getDvdLibrary() {
        return dao.getAllDvds();
    }

    @RequestMapping(value = "/dvd/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDVD(@PathVariable("id") int dvdId) {
        dao.deleteDvd(dvdId);
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String displayAddForm() {
        return "add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Dvd addDvd(@Valid @RequestBody Dvd newDvd) {
        dao.addDvd(newDvd);
        return newDvd;
    }
    
    @RequestMapping(value = "/dvd/{dvdId}", method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateDVD(@PathVariable("dvdId") int dvdId, @Valid @RequestBody Dvd edittedDvd) {
                    edittedDvd.setId(dvdId);
                    dao.updateDvd(edittedDvd);
    }
    
    @RequestMapping(value = "/dvd/{dvdId}", method=RequestMethod.GET)
    @ResponseBody
    public Dvd getDvdById(@PathVariable("dvdId") int dvdId) {
        
        return dao.getDvdById(dvdId);
        
    }
    @RequestMapping (value= "/search", method=RequestMethod.GET)
    public String getSearchDisplay() {
        return "search";
    }
    
    @RequestMapping (value= "search/dvds", method=RequestMethod.POST)
    @ResponseBody
    public List<Dvd> searchDvds(@RequestBody Map<String,String> searchMap) {
        
        Map<SearchTerm, String> criteriaMap = new HashMap<>();
        
        String currentTerm = searchMap.get("titleSearch");
        if(currentTerm != null && !currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.TITLE, currentTerm);          
        }
        currentTerm = searchMap.get("yearSearch");
        if(currentTerm != null && !currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.YEAR, currentTerm);
        }
        currentTerm = searchMap.get("categorySearch");
        if(currentTerm != null && !currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.CATEGORY, currentTerm);
        }
        currentTerm = searchMap.get("ratingSearch");
        if(currentTerm != null && !currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.RATING, currentTerm);
        }   
       
        return dao.searchDvd(criteriaMap);
    }
}
