/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdlibrarywithwinona.dao;

import com.swcguild.dvdlibrarywithwinona.model.Dvd;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface DvdLibraryDao {

    Dvd addDvd(Dvd dvd);

    void deleteDvd(int id);

    List<Dvd> getAllDvds();

    Dvd getDvdById(int id);

    void updateDvd(Dvd dvd);
    
    public List<Dvd> searchDvd(Map<SearchTerm, String> criteria);
}
