/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdlibrarywithwinona.dao;

import com.swcguild.dvdlibrarywithwinona.model.Dvd;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DvdLibraryDaoImpl implements DvdLibraryDao {

    Map<Integer, Dvd> dvdLibrary = new HashMap<>();
    private static int idCounter = 0;

    public DvdLibraryDaoImpl() {

        Dvd dvd = new Dvd();
        dvd.setTitle("Star Wars");
        dvd.setYear("1977");
        dvd.setCategory("SciFi");
        dvd.setRating("PG");
        this.addDvd(dvd);

        dvd = new Dvd();
        dvd.setTitle("Pride and Prejudice");
        dvd.setYear("1994");
        dvd.setCategory("Classic");
        dvd.setRating("PG");
        this.addDvd(dvd);

        dvd = new Dvd();
        dvd.setTitle("Space Balls");
        dvd.setYear("1987");
        dvd.setCategory("Comedy");
        dvd.setRating("PG-13");
        this.addDvd(dvd);

        dvd = new Dvd();
        dvd.setTitle("Titanic");
        dvd.setYear("1997");
        dvd.setCategory("Drama");
        dvd.setRating("PG-13");
        this.addDvd(dvd);

        dvd = new Dvd();
        dvd.setTitle("Raiders of the Lost Arc");
        dvd.setYear("1981");
        dvd.setCategory("Action");
        dvd.setRating("PG");
        this.addDvd(dvd);

        dvd = new Dvd();
        dvd.setTitle("Double Jeopardy");
        dvd.setYear("1999");
        dvd.setCategory("Adventure");
        dvd.setRating("R");
        this.addDvd(dvd);

    }

    @Override
    public List<Dvd> getAllDvds() {
        return new ArrayList(dvdLibrary.values());
    }

    @Override
    public Dvd getDvdById(int id) {
        return dvdLibrary.get(id);
    }

    @Override
    public Dvd addDvd(Dvd dvd) {
        dvd.setId(idCounter);
        idCounter++;
        dvdLibrary.put(dvd.getId(), dvd);
        return dvd;

    }

    @Override
    public void deleteDvd(int id) {
        dvdLibrary.remove(id);
    }

    @Override
    public void updateDvd(Dvd dvd) {
        dvdLibrary.put(dvd.getId(), dvd);
    }

    @Override    
    public List<Dvd> searchDvd(Map<SearchTerm, String> criteria) {

        String titleCriteria = criteria.get(SearchTerm.TITLE);
        String yearCriteria = criteria.get(SearchTerm.YEAR);
        String categoryCriteria = criteria.get(SearchTerm.CATEGORY);
        String ratingCriteria = criteria.get(SearchTerm.RATING);

        Predicate<Dvd> titlePred;
        Predicate<Dvd> yearPred;
        Predicate<Dvd> categoryPred;
        Predicate<Dvd> ratingPred;

        Predicate<Dvd> allPass = (dvd) -> {
            return true;
        };

        titlePred = (titleCriteria == null || titleCriteria.isEmpty())
                ? allPass : (dvd) -> dvd.getTitle().toLowerCase().contains(titleCriteria.toLowerCase());
        yearPred = (yearCriteria == null || yearCriteria.isEmpty())
                ? allPass : (dvd) -> dvd.getYear().toLowerCase().contains(yearCriteria.toLowerCase());
        categoryPred = (categoryCriteria == null || categoryCriteria.isEmpty())
                ? allPass : (dvd) -> dvd.getTitle().toLowerCase().contains(categoryCriteria.toLowerCase());
        ratingPred = (ratingCriteria == null || ratingCriteria.isEmpty())
                ? allPass : (dvd) -> dvd.getTitle().toLowerCase().contains(ratingCriteria.toLowerCase());

        return dvdLibrary.values().stream()
                .filter(titlePred)
                .filter(yearPred)
                .filter(categoryPred)
                .filter(ratingPred)
                .collect(Collectors.toList());
    }
}
