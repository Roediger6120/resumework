$(document).ready(function () {


    $('#add-button').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'add',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json',
            data:JSON.stringify({
                title:$('#add-dvd-title').val(),
                year:$('#add-dvd-year').val(),
                category:$('#add-dvd-category').val(),
                rating:$('#add-dvd-rating').val()
            })
        }).success (function() {
//            alert("Dvd Added");
            $('#add-dvd-title').val('');
            $('#add-dvd-year').val('');
            $('#add-dvd-category').val('');
            $('#add-dvd-rating').val('');
            alert("Dvd Added");
            // If you want something to happen when it succeeds..
            /// put that code here...!!
        });
             
                
                
    });
});