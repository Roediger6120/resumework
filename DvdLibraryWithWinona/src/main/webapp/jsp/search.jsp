<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dvd Library</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Dvd Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">

                    <li role="presentation"><a href="${pageContext.request.contextPath}/">Library</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/add">Add</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                </ul>    
            </div>

        </div>
        <div class="container"> 
            <div class="col-md-5">
                <h3>Search Results</h3>

                <table class="table table-bordered" id ="alldvd">

                    <tr>
                        <th width="5%"></th>
                        <th width="5%"></th>
                        <th width="20%">Title</th>
                        <th width="10%">Year</th>
                        <th width="15%">Category</th>
                        <th width="5%">Rating</th>
                    </tr>
                    <tbody id="contentRows"></tbody>

                </table>
            </div>


            <div class="col-md-5 col-md-offset-1">
                <h2>Search DVDs</h2>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="search-dvd-title" class="col-md-2 control-label">Title:</label>
                        <div class="col-md-4"><input type="text"  id="search-dvd-title" placeholder="Title"/></div>
                    </div>
                    <div class="form-group">
                        <label for="search-dvd-year" class="col-md-2 control-label">Year:</label>
                        <div class="col-md-4"><input type="text" class="form-control" id="search-dvd-year" placeholder="Year"/></div>
                    </div>
                    <div class="form-group">
                        <label for="search-dvd-category" class="col-md-2 control-label">Category</label>
                        <div class="col-md-4"><input type="text" class="form-control" id="search-dvd-category" placeholder="Category"/></div>
                    </div>
                    <div class="form-group">
                        <label for="search-dvd-rating" class="col-md-2 control-label">Rating</label>
                        <div class="col-md-4"><input type="text" class="form-control" id="search-dvd-rating" placeholder="Rating"/></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-2">
                            <button type="submit" id="search-button" class="btn btn-success">Search Dvds</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>    





        <%@include file="scriptsFragment.jsp" %>
        <script src="${pageContext.request.contextPath}/js/dvdlibrary.js"></script>
    </body>
</html>

