$(document).ready(function () {
    loadTable();

    $('#edit-button').click(function (event) {

        event.preventDefault();
//     $('#editModal').modal('toggle');
        $.ajax({
            type: 'PUT',
            url: 'dvd/' + $('#edit-dvdId').val(),
            data: JSON.stringify({
                id: $('#edit-dvdId').val(),
                title: $('#edit-dvd-title').val(),
                year: $('#edit-dvd-year').val(),
                category: $('#edit-dvd-category').val(),
                rating: $('#edit-dvd-rating').val(),
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dateType': 'json'
        }).success(function (data, status) {
            loadTable();
        });
    });
    $('#search-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'search/dvds',
            data: JSON.stringify({
                titleSearch: $('#search-dvd-title').val(),
                yearSearch: $('#search-dvd-year').val(),
                categorySearch: $('#search-dvd-category').val(),
                ratingSearch: $('#search-dvd-rating').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            displayResults(data,status)
        });
    });

});

function loadTable() {


    $.ajax({
        type: 'GET',
        url: 'library',
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (dvdList, status) {
        displayResults(dvdList, status);
});
}

function deleteDvd(id) {
    var answer = confirm("Do you really want to delete this Dvd?");

    if (answer === true) {

        $.ajax({
            type: 'DELETE',
            url: 'dvd/' + id

        }).success(function () {
            loadTable();

        });

    }
}
 function displayResults(dvdList, status) {
    var dTablebody = $('#contentRows');
        dTablebody.empty();
        $.each(dvdList, function (index, dvd) {
            dTablebody.append($('<tr>')
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'onClick': 'deleteDvd(' + dvd.id + ')'
                                    }).text('Delete')
                                    ))
                    .append($('<td>')
                            .append($('<a>')
                                    .attr({
                                        'data-dvd-id': dvd.id,
                                        'data-toggle': 'modal',
                                        'data-target': '#editModal'
                                    }).text('Edit')
                                    ))
                    .append($('<td>')
                            .text(dvd.title)
                            )
                    .append($('<td>')
                            .text(dvd.year)
                            )
                    .append($('<td>')
                            .text(dvd.category)
                            )
                    .append($('<td>')
                            .text(dvd.rating)
                            )
                    );
        });
    }
//function editDvd() {
//    $('#editModal').modal('toggle');
//
//
//
//    var element = $(event.relatedTarget);
//    var dvdId = element.data('dvd-id');
//
//    var modal = $(this);
//
//    $.ajax({
//        type: 'GET',
//        url: 'dvd/' + dvdId,
//        headers: {
//            'Accept': 'application/json'
//        }
//    }).success(function (dvd) {
//
//        modal.find('#dvd-id').text(dvd.dvdId);
//        modal.find('#edit-Title').val(dvd.title);
//        modal.find('#edit-year').val(dvd.year);
//        modal.find('#edit-category').val(dvd.category);
//        modal.find('#edit-rating').val(dvd.rating);
//
//        modal.find('#edit-dvdId').val(dvd.dvdId);
//    });

//}
$('#editModal').on('show.bs.modal', function (event) {

    var element = $(event.relatedTarget);
    var dvdId = element.data('dvd-id');

    var modal = $(this);

    $.ajax({
        type: 'GET',
        url: 'dvd/' + dvdId,
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (dvd) {

        modal.find('#dvd-id').text(dvd.id);
        modal.find('#edit-dvd-title').val(dvd.title);
        modal.find('#edit-dvd-year').val(dvd.year);
        modal.find('#edit-dvd-category').val(dvd.category);
        modal.find('#edit-dvd-rating').val(dvd.rating);
        modal.find('#edit-dvdId').val(dvd.id);

        });
    });
    


