
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog"
             aria-labelledby="editModalLabel" aria-hidden="true">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="editModalLabel">Edit Dvd</h4>
                    </div>
                    <div class="modal-body">
                         
                        <div class="container">
                            <div class="col-md-16">
                                 <h3 id="dvd-id">DvdId Goes Here</h3>
                                <form class="form-horizontal" role="form"
                                      action ="edit" method="POST">
                                    <div class="form-group">
                                        <label form="edit-dvd-title" class="col-med-6 control-label">Title</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" id="edit-dvd-title" name="title" placeholder="Dvd Title">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label form="edit-dvd-year" class="col-med-6 control-label">Year</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" id="edit-dvd-year" name="year" placeholder="Dvd Year">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label form="edit-dvd-category" class="col-med-6 control-label">Category</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" id="edit-dvd-category" name="category" placeholder="Dvd Category">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label form="edit-dvd-rating" class="col-med-6 control-label">Title</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" id="edit-dvd-rating" name="rating" placeholder="Dvd Rating">
                                        </div>
                                    </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-6">
                                    <button type="submit" id="edit-button" class="btn btn-default" data-dismiss="modal">Edit Dvd</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" id="edit-dvdId">
                                </div>
                            </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                      
