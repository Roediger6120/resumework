<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dvd Library</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Dvd Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/">Library</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/add">Add</a></li>
                    
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                </ul>    
            </div>

                    <%@include file="tableFragment.jsp" %>
            </div>
        
            <%@include file="editFragment.jsp" %>
            <%@include file="scriptsFragment.jsp" %>
            <script src="${pageContext.request.contextPath}/js/dvdlibrary.js"></script>
    </body>
</html>

