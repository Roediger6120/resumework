<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Dvd Library</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Dvd Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    
                    <li role="presentation"><a href="${pageContext.request.contextPath}/">Library</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/add">Add</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                </ul>    
            </div>

        </div>
        <div class="container">
            <h3>Add Dvd</h3>
            <div class="col-md-12">
                <form class="form-horizontal" role="form"
                      action="add" method="POST">

                    <div class="form-group">
                        <label form ="add-dvd-title" class="col-md-4 control-label">Title</label>
                        <div class ="col-md-8">
                            <input type ="text" class="form-control" id="add-dvd-title" name="title" placeholder="Dvd Title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label form ="add-dvd-year" class="col-md-4 control-label">Year</label>
                        <div class ="col-md-8">
                            <input type ="text" class="form-control" id="add-dvd-year" name="year" placeholder="Dvd Year"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label form ="add-dvd-category" class="col-md-4 control-label">Category</label>
                        <div class ="col-md-8">
                            <input type ="text" class="form-control" id="add-dvd-category" name="category" placeholder="Dvd Category"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label form ="add-dvd-rating" class="col-md-4 control-label">Rating</label>
                        <div class ="col-md-8">
                            <input type ="text" class="form-control" id="add-dvd-rating" name="rating" placeholder="Dvd Rating"/>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit" id="add-button" class="btn btn-success">Add Dvd</button>
                        </div>   
                    </div>
                </form>
                <%@include file="editFragment.jsp" %>
            </div>
        </div>
        <%@include file="scriptsFragment.jsp" %>
        <script src="${pageContext.request.contextPath}/js/dvdAddForm.js"></script>
    </body>
</html>

