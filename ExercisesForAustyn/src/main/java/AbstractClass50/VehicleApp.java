/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractClass50;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class VehicleApp {

    public static void main(String[] args) {
        List<Vehicle> vehicles = new ArrayList<>();

        MotorizedVehicle vwBug = new MotorizedVehicle("Andrew", 4, 32, "1.6L A16A1 I4, 88 hp 66 kW");
        vehicles.add(vwBug);
        vehicles.add(new MotorizedVehicle("Austyn", 4, 20, "VWAirCooled"));
        vehicles.add(new MotorizedVehicle("Rene", 4, 30, "88 kW Permanent Magnet AC Synchronous"));
        vehicles.add(new MotorizedVehicle("Andrew", 4, 32, "1.6L A16A1 I4, 88 hp 66 kW"));
        vehicles.add(new Bicycle("Winona"));
        vehicles.add(new Bicycle("Solomon"));
       
       for (Object c: vehicles)     
            if (c instanceof Bicycle) {
                Bicycle b = (Bicycle) c;
                System.out.print("\nType of vehicle:  Bicycle, is owned by: " + b.getOwnersName() + " number of wheels "
                        + b.getNumWheels());
            } else if (c instanceof MotorizedVehicle) {
                MotorizedVehicle motorVehicle = (MotorizedVehicle) c;
                System.out.println("\nType of vehicle: MotorizedVehicle, is owned by: " + motorVehicle.getOwnersName() + " number of wheels "
                        + motorVehicle.getNumWheels() + " gas mileage: " + motorVehicle.getMpg() + " engine type: " + motorVehicle.getEngineType());

            }
        }
    }


