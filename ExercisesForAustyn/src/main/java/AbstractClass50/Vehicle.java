/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractClass50;

/**
 *
 * @author apprentice
 */
abstract public class Vehicle {
    protected String ownersName;
    protected int numWheels;

//    public Vehicle(){
//        
//    }
    
    public Vehicle(String ownersName, int numWheels) {
        this.ownersName = ownersName;
        this.numWheels = numWheels;
    }

    
    public String getOwnersName() {
        return ownersName;
    }

    public void setOwnersName(String ownersName) {
        this.ownersName = ownersName;
    }

    public int getNumWheels() {
        return numWheels;
    }

    public void setNumWheels(int numWheels) {
        this.numWheels = numWheels;
    }
    
}
