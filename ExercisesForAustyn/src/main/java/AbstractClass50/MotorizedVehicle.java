/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AbstractClass50;

/**
 *
 * @author apprentice
 */
public class MotorizedVehicle extends Vehicle {
    private int mpg;
    private String engineType;
    
    public MotorizedVehicle(String ownersName, int numWheels, int mpg, String engineType) {
        super(ownersName, numWheels);
//        super();
//        this.ownersName = ownersName;
//        this.numWheels = numWheels;
        this.mpg = mpg;
        this.engineType = engineType;
        
    }

    public int getMpg() {
        return mpg;
    }

    public void setMpg(int mpg) {
        this.mpg = mpg;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }

    
}
