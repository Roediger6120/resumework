/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Abstract52;

/**
 *
 * @author apprentice
 */
public class GraduateSchool extends CollegeApplicant{
    
    private String collegeOrigin;
    
    public GraduateSchool(String name, String college, String collegeOrigin) {
        super(name,college);
        setCollegeOrigin(collegeOrigin);
        
    }

    public String getCollegeOrigin() {
        return collegeOrigin;
    }

    public void setCollegeOrigin(String collegeOrigin) {
        this.collegeOrigin = collegeOrigin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }
    public String matchingCollege(String collegeOrigin) {
        if (collegeOrigin.equalsIgnoreCase(college))
            return "from inside";
        else
            return "from outside";
    }
}


