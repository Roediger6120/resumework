/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Abstract52;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class ApplicantApp {
    public static void main(String[] args) {
        
        ArrayList<CollegeApplicant> collegeApplicants = new ArrayList<>();
        
        collegeApplicants.add(new Undergraduate ("Winona", "Tiffin", 1200, 3.9));
        collegeApplicants.add(new Undergraduate ("Solomon", "Ohio State", 1000, 3.2));
        collegeApplicants.add(new GraduateSchool ("Austyn", "Yale", "MIT"));
        collegeApplicants.add(new GraduateSchool ("Rene", "UofL", "UK"));
        collegeApplicants.add(new GraduateSchool ("Patrick", "USC", "USC"));
        
        for (CollegeApplicant ca: collegeApplicants) {
            if (ca instanceof Undergraduate) {
                Undergraduate u = (Undergraduate)ca;
                System.out.println("Name: " + u.getName() + " College: " + u.getCollege() + " Sat: " + u.getSatScore()
                + " GPA: " + u.getGpa());
            } else if(ca instanceof GraduateSchool) {
                GraduateSchool gs = (GraduateSchool)ca;
                System.out.println("Name: " + gs.getName() + " College applying to: " + gs.getCollege() + " College from: " +
                       gs.getCollegeOrigin() + " " + gs.matchingCollege(gs.getCollegeOrigin()));
                
                
            }
        }
    }
}
