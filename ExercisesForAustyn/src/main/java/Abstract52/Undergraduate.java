/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Abstract52;

/**
 *
 * @author apprentice
 */
public class Undergraduate extends CollegeApplicant{
    private int satScore;
    private double gpa;
    
    public Undergraduate (String name, String college, int satScore, double gpa) {
        super(name, college);
        setSatScore(satScore);
        setGpa(gpa);
    }

    public int getSatScore() {
        return satScore;
    }

    public void setSatScore(int satScore) {
        this.satScore = satScore;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }
    
}
