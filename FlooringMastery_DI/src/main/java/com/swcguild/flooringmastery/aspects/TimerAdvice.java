/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.flooringmastery.aspects;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 *
 * @author apprentice
 */
public class TimerAdvice {
    
    public Object timeMethod(ProceedingJoinPoint jp) {
        Object ret = null;
        try{
            
            long start = System.currentTimeMillis();
            ret = jp.proceed();
            long end = System.currentTimeMillis();
            System.out.println("*+*+*+*+*+*+*+*+*+*+*+*+*+");
            String methodBefore = jp.getSignature().getName();
            System.out.println(methodBefore + " took " + (end - start) + "ms");
             System.out.println("*+*+*+*+*+*+*+*+*+*+*+*+*+");
             
        } catch (Throwable ex) {
            System.out.println("exception happend in timer");
        }
        
        return ret;
    }
}
