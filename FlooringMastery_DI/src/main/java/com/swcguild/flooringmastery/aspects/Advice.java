/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.flooringmastery.aspects;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 *
 * @author apprentice
 */
public class Advice {

    public void addDateAndTime() {
        LocalDate date = LocalDate.now();
        LocalTime time = LocalTime.now();
        
        System.out.println("The order was changed " + date + " " + time);
    }
}
