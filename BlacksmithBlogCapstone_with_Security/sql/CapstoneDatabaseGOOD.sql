DROP DATABASE IF EXISTS Blacksmith;
CREATE DATABASE Blacksmith;
USE Blacksmith;

CREATE TABLE BlogEntries (
    id int(11) AUTO_INCREMENT NOT NULL,
    author varchar (50),
    blogTimestamp timestamp,
    title varchar(50),
    blogBody longtext,
	category varchar(50),
    approvalStatus varchar(20),
    expirationDate datetime,
    PRIMARY KEY (id)
);

CREATE TABLE Comments (
    id int(11) AUTO_INCREMENT NOT NULL,
    body longtext,
    cName varchar(50),
    commentTimestamp timestamp,
    blogEntryId int(11),
    PRIMARY KEY (id),
    FOREIGN KEY (blogEntryId)
        REFERENCES BlogEntries (id)
);

CREATE TABLE Keywords (
    id int(11) AUTO_INCREMENT NOT NULL,
    word varchar(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Categories (
    id int(11) AUTO_INCREMENT NOT NULL,
    category varchar(100) NOT NULL,
	blogEntryId int(11),
	FOREIGN KEY (blogEntryId) REFERENCES BlogEntries (id),
	PRIMARY KEY (id)
);

CREATE TABLE BlogEntriesKeywords (
    keywordId int(11),
    blogEntryId int(11),
    FOREIGN KEY (keywordId)
        REFERENCES Keywords (id),
    FOREIGN KEY (blogEntryId)
        REFERENCES BlogEntries (id)
);

CREATE TABLE BlogEntriesCategories (
    categoryId int(11),
    blogEntryId int(11),
    FOREIGN KEY (categoryId)
        REFERENCES Categories (id),
    FOREIGN KEY (blogEntryId)
        REFERENCES BlogEntries (id)
);


-- CREATE TABLE BlogSummaries (
-- blogEntryId int(11),
-- title varchar (100),
-- author varchar (50),
-- bDate timestamp,
-- category varchar(100),
-- 
-- 
-- FOREIGN KEY (blogEntryId) REFERENCES BlogEntries (id) ON DELETE CASCADE,
-- FOREIGN KEY (title) REFERENCES BlogEntries (title) ON DELETE CASCADE,
-- FOREIGN KEY (author) REFERENCES BlogEntries (author) ON DELETE CASCADE,
-- FOREIGN KEY (bdate) REFERENCES BlogEntries (blogTimestamp) ON DELETE CASCADE,
-- FOREIGN KEY (category) REFERENCES Categories (category) ON DELETE CASCADE
-- 
-- );
-- 

INSERT INTO BlogEntries (id, author, blogTimeStamp, title, blogBody, category, approvalStatus, expirationDate) VALUES (1, 'Winona', '2016-04-19 03:14:36', 'The Best Steel Swords',
 '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
 ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
 Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'swords','approved', '2016-12-12');

INSERT INTO BlogEntries (author, blogTimeStamp, title, blogBody, category, approvalStatus, expirationDate) VALUES ('Solomon', '2016-04-18 02:13:46', 'Is BlackSmithing Obsolete?',
 '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, 
eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, 
eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, 
eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo</p>', 'superman', 'approved', '2016-12-19');

INSERT INTO BlogEntries (author, blogTimeStamp, title, blogBody, category, approvalStatus, expirationDate) VALUES ('Patrick', '2016-02-18 11:19:22', 'How to Test Your Materials',
 '<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione 
voluptatem sequi nesciunt.Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui rationeNemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui rationeNemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, 
sed quia consequuntur magni dolores eos qui ratione</p>', 'shrimp','approved', '2016-10-19');

INSERT INTO BlogEntries (author, blogTimeStamp, title, blogBody, category, approvalStatus, expirationDate) VALUES ('Rene', '2016-01-23 10:29:22', 'How to Talk With an Accent',
 '<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore 
magnam aliquam quaerat voluptatem. Croissant brownie pastry bonbon carrot cake jelly-o lollipop tootsie roll sweet. Halvah cupcake croissant icing tootsie roll chocolate gummies toffee. Cake liquorice fruitcake. Dessert cheesecake lollipop tart. Apple pie brownie cheesecake halvah chocolate bar chupa chups candy toffee. Bonbon sesame snaps chocolate bar jujubes tootsie roll danish liquorice pudding danish. Apple pie marzipan cookie cupcake chupa chups oat cake fruitcake chocolate. Bonbon lollipop brownie danish cotton candy dragée marzipan
 topping pudding. Cake apple pie cotton candy pie danish sesame snaps bear claw topping topping. Icing sweet roll pudding sesame snaps. Biscuit marshmallow pastry chocolate gummies. Cake danish cookie. Gingerbread gummi bears pudding topping wafer bear claw. Marshmallow chocolate bar cotton candy pie brownie chupa chups muffin. Fruitcake candy canes cookie ice cream apple pie cotton candy sugar plum liquorice muffin. Tart macaroon bonbon.</p>', 'star wars', 'approved', '2016-09-19');

INSERT INTO Comments (id, cName, commentTimeStamp, blogEntryId, body) VALUES(1, 'Solomon', '2016-04-19 03:35:56', 1, 'Google Earth satellite technology gives scientists and environmentalists a way to measure and visualize changes of the world on both land and water. This technology can have great impact on monitoring endangered animal populations around the world. For example, with the help of <a href="http://www.globalforestwatch.org/">Global Forest Watch</a>, powered by <a href="https://earthengine.google.com/">Google Earth Engine</a>, scientists at the University of Minnesota are suggesting that <a href="http://blog.globalforestwatch.org/2016/04/we-can-save-tigers-from-extinction-with-a-little-help-from-satellites/?utm_campaign=tiger_habitat&amp;utm_source=gif&amp;utm_medium=caption">wild tiger populations may rebound by 2022</a>, due to the efforts to restore tiger habitats in key regions.<br />
<div class="separator" style="clear: both; text-align: center;"><a href="https://3.bp.blogspot.com/-V3jRTg6OxCM/Vw7YbvX7iYI/AAAAAAAAE0s/m7XXLJg3lSE-aycJGUyPNDgjZ_4eTbQVgCLcB/s1600/Screen%2BShot%2B2016-04-06%2Bat%2B10.26.39%2BAM.png" imageanchor="1" style="margin-left: 1em; margin-right: 1em;"><img border="0" height="366" src="https://3.bp.blogspot.com/-V3jRTg6OxCM/Vw7YbvX7iYI/AAAAAAAAE0s/m7XXLJg3lSE-aycJGUyPNDgjZ_4eTbQVgCLcB/s640/Screen%2BShot%2B2016-04-06%2Bat%2B10.26.39%2BAM.png" width="640" /></a></div><i><a href="http://www.globalforestwatch.org/map/5/17.89/92.30/ALL/grayscale/loss,forestgain/619?tab=countries-tab&amp;begin=2001-01-01&amp;end=2015-01-01&amp;threshold=30">Anyone can now view tiger conservation areas (in orange and yellow above) using Global Forest Watch.</a></i><br />
');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Carrie', '2016-04-19 03:35:56', 1, 'As of this week, Sunroof expanded to 42 states across the U.S. (from <a href="http://googlegreenblog.blogspot.com/2015/12/should-you-go-solar-just-ask-project.html">10 states in December</a>), which makes imagery and data available for a solar analysis to 43 million rooftops. We&#8217;re also working with organizations like Sierra Club and their <a href="http://www.sierraclub.org/ready-for-100">Ready for 100 campaign</a> to help analyze the solar potential of cities across the U.S.');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Andrew', '2016-04-20 00:38:56', 1, 'For the past few years, <a href="//www.google.com/earth/outreach/index.html">Google Earth Outreach</a> and the <a href="https://www.edf.org/climate/methanemaps">Environmental Defense Fund</a> (EDF) have been working together to map methane leaks from natural gas pipelines under our streets. Since methane is a very potent greenhouse gas (GHG), even small leaks can add up to big emissions that can hurt our climate. By attaching methane analyzers to select Street View cars, we&#8217;ve driven more than 7,500 miles and have <a href="https://www.edf.org/climate/methanemaps">mapped 4,200+ leaks in 10 cities</a>. What we found ranges from an average of one leak per mile (in Boston) to one leak every 200 miles (in Indianapolis), demonstrating the effectiveness of techniques like using plastic piping instead of steel for pipeline construction. We hope utilities can use this data to prioritize the replacement of gas mains and service lines (like New Jersey&#8217;s <a href="https://www.pseg.com/info/media/newsreleases/2015/2015-11-16.jsp#.Vw7Y15MrKqD">PSE&amp;G announced</a> last fall). We&#8217;re also partnering with <a href="https://aclima.io/">Aclima</a> to measure <a href="http://googlegreenblog.blogspot.com/2015/09/making-invisible-visible-by-mapping-air.html">many more pollutants</a> with Street View cars in California communities through this year.	');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Winona', '2016-04-20 04:20:00', 2, '<i>Anyone can explore the maps at <a href="https://www.edf.org/climate/methanemaps">www.edf.org/methanemaps</a>.</i></div><br />
Technology is crucial to increasing energy efficiency, raising climate change awareness, and sustainability efforts. To learn more about what you can do to help, take a moment to explore our&nbsp;<a href="//www.google.com/earth/outreach/index.html">Google Earth Outreach</a>&nbsp;site,&nbsp;where these tools and more are described in depth.<br />');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Patrick', '2016-04-20 12:12:12', 2, 'Google Earth satellite technology gives scientists and environmentalists a way to measure and visualize changes of the world on both land and water. This technology can have great impact on monitoring endangered animal populations around the world. For example, with the help of <a href="http://www.globalforestwatch.org/">Global Forest Watch</a>');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Rene', '2016-04-21 13:13:13', 3, 'Taking Google Earth imagery and overlaying annual sun exposure and weather patterns, Sunroof is able to assess viable roof space for solar panel installation, estimate the value of solar and savings based on local energy costs, and connect you with providers of solar panels in your area.');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Austyn', '2016-04-21 14:14:14', 3, 'Puerto Rico is the shining star of the Carribean.');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Erin', '2016-04-19 15:15:15', 3, 'The Swiss Superman says he spoke to Shane, and the match has been changed');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Jacob', '2016-04-23 16:16:16', 3, 'Another long bout with both men going all out. Lots of nearfalls, but eventually ends when Dean gets his knees up on KOCategoriesCategoriess second frog splash, allowing Ambrose to hit Dirty Deeds.');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Jason', '2016-04-23 17:17:17', 3, 'WWE Raw results, live blog (April 18, 2016): London calling');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Alex', '2016-04-23 18:18:18', 4, 'As in, live from the States');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Laura', '2016-04-24 19:19:19', 4, 'REMINDER: NO GIFS OR PICS ARE ALLOWED IN THE COMMENTS SECTION. OFFENDERS WILL BE BANNED.The comments section here should be kept SPOILER FREE.');
INSERT INTO Comments (cName, commentTimeStamp, blogEntryId, body) VALUES('Mike', '2016-04-25 20:20:22', 4, 'See how much they editing out Reigns’ boos within a few hours');

INSERT INTO Categories (id, category) VALUES(1, 'Swords');
INSERT INTO Categories (category) VALUES('Shields');
INSERT INTO Categories (category) VALUES('Steel');
INSERT INTO Categories (category) VALUES('Armor');

CREATE TABLE IF NOT EXISTS users (
 user_id int(11) NOT NULL AUTO_INCREMENT,
 username varchar(20) NOT NULL,
 password varchar(20) NOT NULL,
 enabled tinyint(1) NOT NULL,
 PRIMARY KEY (user_id),
 KEY username (username)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;
--
-- Dumping data for table `users`
--
INSERT INTO users (user_id, username, password, enabled) VALUES
(1, 'test', 'test', 1),
(2, 'test1', 'test1', 1),
(3, 'test2', 'test2', 1),
(4, 'test3', 'test3', 1);
--
-- Table structure for table `authorities`
--
CREATE TABLE IF NOT EXISTS authorities (
 username varchar(20) NOT NULL,
 authority varchar(20) NOT NULL,
 KEY username (username)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Dumping data for table `authorities`
--
INSERT INTO authorities (username, authority) VALUES
('test', 'ROLE_ADMIN'),
('test1', 'ROLE_GUEST'),
('test2', 'ROLE_ANONYMOUS'),
('test3', 'ROLE_USER');
--
-- Constraints for table `authorities`
--
ALTER TABLE authorities
 ADD CONSTRAINT authorities_ibfk_1 FOREIGN KEY (username) REFERENCES users (username);


