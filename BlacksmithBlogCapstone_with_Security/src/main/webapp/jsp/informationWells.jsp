<div class="col-md-4">
    <hr></hr>
    <!-- Blog Search Well -->
    <div class="well">
        <h4>Blog Search</h4>
        <div class="input-group">
            <input id="global-search-term" type="text" class="form-control">
            <span class="input-group-btn">
                <button button id="global-search-button" class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
        </div>
        <!-- /.input-group -->
    </div>
    
    <!-- Side Widget Well -->
    <div class="well">
        <h4>Popular Posts</h4>
        <div class="row">
            <div class="col-lg-12">
                <table id="popularRowsTable" class="table table-hover">
                    <tr><th>Title</th><th>Comments</th></tr>
                    <tbody id="popularRows"></tbody>
                </table>
            </div>

        </div>
    </div>

    <!-- Blog Categories Well -->
    <div class="well">
        <h4>Blog Categories</h4>
        <div class="row">
            <div class="col-lg-12">                            
                <div id="blogCategories">Fill Categories</div>                            
            </div>

        </div>
        <!-- /.row -->
    </div>

    <!-- Side Widget Well -->
    <div class="well">
        <h4>Archives</h4>
        <li><a href="/BlacksmithBlogCapstone/archives">View Archives</a>
        </li>

    </div>
</div>