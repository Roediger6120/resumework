<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Blacksmith Blog">
        <meta name="author" content="Andrew Carrie Solomon">
        <title>Contact</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/blog-post.css" rel="stylesheet">
    </head>
    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="${pageContext.request.contextPath}">Blog</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <%@include file="navBarFragment.jsp"%>
                </div>
                <!-- /.navbar-collapse -->

            </div>

            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">
            <div class="wide">
                <div class="col-xs-5 line"><hr></div>
                <div class="col-xs-2 logo"></div>
                <div class="col-xs-5 line"><hr></div>
            </div>

            <div class="row">

                <!-- Blog Post Content Column -->
                <div class="col-lg-8">

                    <!-- Blog Post -->

                    <!-- Title -->

                    <div class="col-md-12">

                        <h1>Contact Us</h1>
                        <br>
                        <form class="form-horizontal" role="form" action="MAILTO:ahill@thesoftwareguild.com" method="post" enctype="text/plain">
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Name:</label>
                                <div class="col-md-8"><input type="text" class="form-control" id="name" placeholder="Name"/></div>
                            </div>                            

                            <div class="form-group">
                                <label for="e-mail" class="col-md-4 control-label">Email</label>
                                <div class="col-md-8"><input type="email" class="form-control" id="email" placeholder="Email"/></div>
                            </div>
                            <div class="form-group">
                                <label for="comments" class="col-md-4 control-label">Comments</label>
                                <div class="col-md-8"><textarea style="width:100%" id="comments" name="Comments" rows="10"></textarea></div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit" id="contact-button" class="btn btn-default">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <hr>



                </div>



            </div>
            <!-- /.row -->

            <hr>

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; Your Website 2014</p>
                    </div>
                </div>
                <!-- /.row -->
            </footer>

        </div>
        <!-- /.container -->       

   
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/blogEntries.js"></script>
  </body>
</html>

