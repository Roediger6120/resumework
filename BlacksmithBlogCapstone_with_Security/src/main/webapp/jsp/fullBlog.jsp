<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Blacksmith Blog">
        <meta name="author" content="Andrew Carrie Solomon">
        <title>Brunhilde's Blacksmith Blog</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="${pageContext.request.contextPath}/css/blog-post.css" rel="stylesheet">
        <script src="https://cdn.tinymce.com/4/tinymce.min.js"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: '#edit-blog-content', // change this value according to your HTML
                plugins: 'code',
                a_plugin_option: true,
                a_configuration_option: 400
            });
        </script>        
    </head>
    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="${pageContext.request.contextPath}">Blog</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <%@include file="navBarFragment.jsp"%>
                </div>
                <!-- /.navbar-collapse -->

            </div>

            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">
            <div class="wide">

                <div class="col-xs-2 logo">Logo</div>
                <div class="col-xs-5 line"><hr></div>
            </div>

            <div class="row">

                <!-- Blog Post Content Column -->
                <div class="col-lg-8">

                    <!-- Blog Post -->

                    <!-- Title -->
                    <h1>Blacksmith Blog</h1>

                    <!-- Author -->
                    <p class="lead">
                        by <a href="${pageContext.request.contextPath}/about">Shop Owner</a>
                    </p>

                    <hr>

                    <div id="editBlogForm" class="collapse">
                        <sf:form modelAttribute="blogFound" method="post">                            
                            <fieldset class="form-group">
                                <label for="blogTitle">Title</label>
                                <input type="text" id="edit-title" class="form-control" path="title" value="${blogFound.title}">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="blogAuthor">Author</label>
                                <input type="text" id="edit-author" class="form-control" path="author" value="${blogFound.authorName}">
                            </fieldset>
                            <fieldset class="form-group">
                                <label for="blogCategory">Category</label>
                                <input type="text" id="edit-category" class="form-control" path="category" value="${blogFound.category}">
                            </fieldset>
                            <textarea class="form-control" id="edit-blog-content" rows="10" value=${blogFound.body}></textarea>
                            <input type="hidden" id="blogId" value=${blogFound.id}>
                            <button id="edit-blog-button" type="submit" class="btn btn-primary">Submit</button>
                        </sf:form>                    
                    </div>


                    <!-- Blogs generated here -->
                    <div id="fullBlog">
                        <div class="row">
                            <h3 id="test">${blogFound.title}</h3>
                        </div>
                        <div class="row">
                            <p><span class="glyphicon glyphicon-time"></span>${blogFound.timestamp}</p> 
                            <p><span class="glyphicon glyphicon-comment"></span>&nbsp;${blogFound.authorName}</p>
                        </div>
                        </hr>
                        <div class="row">
                            <input type="hidden" id="blogId" value=${blogFound.id}>
                            ${blogFound.body}                            
                        </div>
                    </div>

                    <hr>
                    <div style="background:#D1D1D1;border:1px dotted black;text-align:left;padding:10px">
                        <h3>Add Comment</h3>
                        <div id="addBlogComments">
                            <form class="form-horizontal" role="form">                                
                                <input type="text" id="commentAuthor" class="form-control" placeholder="Name">
                                <textarea class="form-control" id="commentContent" rows="3"></textarea>
                                <input type="hidden" id="blogId" value=${blogFound.id}>
                                <button id="add-comment-button" type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>                        
                    </div>
                    </hr>
                    <div style="background:#D1D1D1;border:1px dotted black;text-align:left;padding:10px">

                        <h3>Comments</h3>
                        <div id="blogComments">
                            Populate Comments                            
                        </div>


                    </div>

                </div>

                <!-- Blog Sidebar Widgets Column -->
                <sec:authorize access="hasRole('ROLE_ADMIN')" var="isAdmin">
                    <c:if test="${isAdmin}">
                        <div class="col-md-4">
                            <div class="well">
                                <h4>Edit Blog</h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <button class="btn btn-warning btn-block" type = "submit" data-toggle="collapse" data-target="#editBlogForm">Edit Blog</button>
                                    </div>                                                                        
                                </div>
                                <!-- /.input-group -->
                            </div>
                        </div>
                    </c:if>
                </sec:authorize>
                <%@include file="informationWells.jsp"%>


            </div>
            <!-- /.row -->

            <hr>

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; Your Website 2014</p>
                    </div>
                </div>
                <!-- /.row -->
            </footer>

        </div>
        <!-- /.container -->       
        <!--
            </body>-->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/blogEntries.js"></script>
    </body>
</html>

