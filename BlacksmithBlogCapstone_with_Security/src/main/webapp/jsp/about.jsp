<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Blacksmith Blog">
        <meta name="author" content="Andrew Carrie Solomon">
        <title>About</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/blog-post.css" rel="stylesheet">
    </head>
    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="${pageContext.request.contextPath}">Blog</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <%@include file="navBarFragment.jsp"%>
                </div>
                <!-- /.navbar-collapse -->

            </div>

            <!-- /.container -->
        </nav>

        <!-- Page Content -->
        <div class="container">
            <div class="wide">
                <div class="col-xs-5 line"><hr></div>
                <div class="col-xs-2 logo"></div>
                <div class="col-xs-5 line"><hr></div>
            </div>
        </div>
        <div class="container">
            <div class="row text-center">                
                <div class="col-lg-5 "><h1>Brunhilde Kardos</h1></div>
                <div class="col-lg-6 col-lg-offset-1"><h1>About Me</h1></div>
                <div class="col-lg-5 "><img src = "img/brunhilde.jpg" alt="Austyn"></div>
                <div class="col-lg-6 col-lg-offset-1">

                    <blockquote>Tacos pinterest craft beer asymmetrical. Bespoke trust fund sustainable, mlkshk pop-up 
                        chicharrones squid cardigan man bun skateboard shabby chic everyday carry tacos.
                        Food truck cold-pressed umami trust fund meh, actually biodiesel yuccie banh mi 
                        pour-over artisan health goth chartreuse. Normcore selfies man bun disrupt, occupy gluten-free sartorial.</blockquote>

                    <blockquote>Etsy neutra meggings, keytar franzen meditation ethical synth seitan letterpress.
                        Scenester affogato deep v pickled biodiesel post-ironic. 8-bit tattooed XOXO, 
                        waistcoat hashtag cardigan fap 90's. Plaid pinterest pabst master cleanse DIY 
                        neutra post-ironic. Paleo celiac knausgaard, next level XOXO listicle butcher 
                        crucifix selvage scenester disrupt helvetica migas pop-up. Biodiesel plaid blue 
                        bottle organic green juice occupy. Chia godard kickstarter yuccie, pork belly 
                        fixie umami franzen literally 90's aesthetic church-key vice williamsburg.</blockquote>

                </div>
            </div>
            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; Your Website 2014</p>
                    </div>
                </div>
                <!-- /.row -->
            </footer>

        </div>
        <!-- /.container -->       


        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/blogEntries.js"></script>
    </body>
</html>

