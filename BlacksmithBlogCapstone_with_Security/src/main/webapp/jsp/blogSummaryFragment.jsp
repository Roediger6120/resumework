<table id="summaryTable" class="table table-hover">
    <tr>
        <th width="10%">Date</th>
        <th width="35%">Title</th>
        <th width="15%">Author</th>
        <th width="8%">Status</th> 
        <th width="8%">Comments</th>         
        <th width="8%">Approve</th>        
        <th width="8%">Delete</th>
    </tr>
    <tbody id="summaryRows"></tbody>
</table>