$(document).ready(function () {
   
    $('#add-comment-button').click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'GET',
            url: '/BlacksmithBlogCapstone/search/' + $('#global-search-term').val(),
            data: JSON.stringify({
                searchTerm: $('#global-search-term').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }

        }).success(function (data, status) {
            //$('#global-search-term').val('');                        
            fillBlogEntries(data, status);

        });

    });
    
    
});

